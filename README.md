# backend

### Install Poetry

On Linux/OSX:

```shell
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```

On Windows:

```powershell
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py -UseBasicParsing).Content | python -
```

### Install the backend

```shell
poetry install
```

### Run the server

```shell
poetry run uvicorn backend.api:app --reload
```

### Run the tests

```shell
poetry run pytest
```
