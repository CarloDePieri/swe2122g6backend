from invoke import task
import os


@task
def test(c, s=False):
    os.environ["PYTHONPATH"] = "."
    capture = ""
    if s:
        capture = " -s"
    c.run(f"poetry run pytest{capture}")


@task
def run(c):
    os.environ["PYTHONPATH"] = "."
    c.run("poetry run uvicorn backend.api:app --reload")


@task
def run_production(c):
    os.environ["PYTHONPATH"] = "."
    c.run(
        "poetry run gunicorn -b 0.0.0.0:8000 -w 4 --worker-class uvicorn.workers.UvicornWorker backend.api:app "
    )


@task
def test_this(c):
    os.environ["PYTHONPATH"] = "."
    c.run("poetry run pytest -s -m runthis")


@task()
def test_cov(c):
    os.environ["PYTHONPATH"] = "."
    if not os.path.isdir("coverage"):
        os.mkdir("coverage")
    folder = os.path.join("coverage", "sonarqube")
    if not os.path.isdir(folder):
        os.mkdir(folder)
    c.run(
        f"poetry run pytest --cov=backend "
        f"--cov-report html:coverage/cov_html "
        f"--cov-report xml:coverage/sonarqube/coverage.xml "
        f"--junitxml=coverage/sonarqube/results.xml",
    )


@task(test_cov)
def html_cov(c):
    c.run("xdg-open coverage/cov_html/index.html")


@task()
def checks(c):
    c.run("poetry run black --check .")
    c.run("poetry run mypy --strict backend")


@task
def ci(c):
    """Launch the gitlab ci pipeline locally.

    This needs gitlab-runner and docker installed; the gitlab-ci-container image must be present.

    Secrets are loaded from a dotenv file: .env"""
    from dotenv import load_dotenv
    import os

    load_dotenv()

    sonarqube_instance = os.getenv("SONARQUBE_INSTANCE")
    sonarqube_login = os.getenv("SONARQUBE_LOGIN")
    key = os.getenv("KEY")
    key_secret = os.getenv("KEY_SECRET")
    access_token = os.getenv("ACCESS_TOKEN")
    access_token_secret = os.getenv("ACCESS_TOKEN_SECRET")
    bearer_token = os.getenv("BEARER_TOKEN")
    test_cache_key = os.getenv("TEST_CACHE_KEY")
    secondary_key = os.getenv("SECONDARY_KEY")
    secondary_key_secret = os.getenv("SECONDARY_KEY_SECRET")
    secondary_access_token = os.getenv("SECONDARY_ACCESS_TOKEN")
    secondary_access_token_secret = os.getenv("SECONDARY_ACCESS_TOKEN_SECRET")
    secondary_bearer_token = os.getenv("SECONDARY_BEARER_TOKEN")

    cmd = (
        "gitlab-runner exec docker "
        f'--env "SONARQUBE_INSTANCE={sonarqube_instance}" '
        f'--env "SONARQUBE_LOGIN={sonarqube_login}" '
        f'--env "KEY={key}" '
        f'--env "KEY_SECRET={key_secret}" '
        f'--env "ACCESS_TOKEN={access_token}" '
        f'--env "ACCESS_TOKEN_SECRET={access_token_secret}" '
        f'--env "BEARER_TOKEN={bearer_token}" '
        f'--env "TEST_CACHE_KEY={test_cache_key}" '
        f'--env "SECONDARY_KEY={secondary_key}" '
        f'--env "SECONDARY_KEY_SECRET={secondary_key_secret}" '
        f'--env "SECONDARY_ACCESS_TOKEN={secondary_access_token}" '
        f'--env "SECONDARY_ACCESS_TOKEN_SECRET={secondary_access_token_secret}" '
        f'--env "SECONDARY_BEARER_TOKEN={secondary_bearer_token}" '
        "--docker-pull-policy if-not-present --docker-image debian-swe-backend test"
    )
    c.run(cmd)


@task()
def clear_cassettes(c):
    from shutil import rmtree

    rmtree("tests/cassettes")
    rmtree("tests/models/cassettes")
    rmtree("tests/saved/cached_stream")
    print("Cleared!")
