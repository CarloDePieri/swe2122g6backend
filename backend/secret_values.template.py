# Copy this into secrets.py and fill in this information
KEY: str = "my_consumer_key"
KEY_SECRET: str = "my_consumer_secret"
ACCESS_TOKEN: str = "my_access_key"
ACCESS_TOKEN_SECRET: str = "my_access_secret"
BEARER_TOKEN: str = "my_bearer_token"

TEST_CACHE_KEY: bytes = b"my16charsecrtkey"

SECONDARY_KEY: str = "my_consumer_key"
SECONDARY_KEY_SECRET: str = "my_consumer_secret"
SECONDARY_ACCESS_TOKEN: str = "my_access_key"
SECONDARY_ACCESS_TOKEN_SECRET: str = "my_access_secret"
SECONDARY_BEARER_TOKEN: str = "my_bearer_token"
