from functools import partial
from json import JSONDecodeError
from platform import python_version
from typing import List, Optional, Dict, TYPE_CHECKING, Callable

import json
from fastapi.encoders import jsonable_encoder
import requests
from requests import Response
from requests_oauthlib import OAuth1

from backend.models import TweetItem
from backend.secrets import Secrets
from backend.twitter import (
    ApiWrapper,
    UserLookupError,
    get_tweet_by_id,
    get_book_competition,
)


class FilteredRawV1Stream:
    """Handle a streaming request to the filtered Twitter v1 api"""

    _rules: Dict[str, str] = {}

    def __init__(self) -> None:
        secrets = Secrets()
        self.auth = OAuth1(
            secrets.secondary_key,
            secrets.secondary_key_secret,
            secrets.secondary_access_token,
            secrets.secondary_access_token_secret,
        )

        user_agent = f"Python/{python_version()} Requests/{requests.__version__}"
        self.session = requests.Session()
        self.session.headers["User-Agent"] = user_agent

    def set_rules(
        self,
        track_keywords: Optional[List[str]] = None,
        follow_usernames: Optional[List[str]] = None,
        locations: Optional[List[float]] = None,
    ) -> None:
        """Set the rules for the filter. Multiple filters combine as OR!"""
        if follow_usernames:
            ids = []
            for user_screen_name in follow_usernames:
                try:
                    username_id = ApiWrapper().get_username_id(user_screen_name)
                    ids.append(username_id)
                except UserLookupError:
                    pass
            self._rules["follow"] = ",".join(map(str, ids))
        if track_keywords:
            self._rules["track"] = ",".join(map(str, track_keywords))
        if locations and len(locations) > 0:
            self._rules["locations"] = ",".join(f"{loc:.4f}" for loc in locations)

    def get_rules(self) -> Dict[str, str]:
        """Return the currently set rules"""
        return self._rules

    def get_stream_response(self) -> Response:
        """Return the streaming response from the Twitter api

        This can then be iterated upon, to get single tweets"""
        method = "POST"
        headers = {"Content-Type": "application/x-www-form-urlencoded"}
        url = f"https://stream.twitter.com/1.1/statuses/filter.json"

        response = self.session.request(
            method,
            url=url,
            headers=headers,
            data=self._rules,
            auth=self.auth,
            stream=True,
        )

        if response.status_code != 200:
            raise CannotGetStreamException(
                "Cannot get stream (HTTP {}): {}".format(
                    response.status_code, response.text
                )
            )

        return response


class CannotGetStreamException(Exception):
    """Raised if a stream response could not be obtained from Twitter"""


def get_v2_tweet_from_v1_data_strategy(
    filters: Dict[str, List[str]],
) -> Callable[[str], Optional[str]]:
    """Compose the strategy with the provided filters; then return the strategy,
    ready to be fed to a ServerSideEventRelay.build_event_source_response"""
    return partial(v2_tweet_from_v1_data_strategy, filters)


def v2_tweet_from_v1_data_strategy(
    filters: Dict[str, List[str]],
    tweet_data_str: str,
) -> Optional[str]:
    """Use the api v1 data string to output a json serialized TweetItem string

    Use the id from the v1 data to query the v2 api, then use the v2 data to build the TweetItem"""
    try:
        tweet_data = json.loads(tweet_data_str)
        tweet_id = str(tweet_data["id_str"])
        tweet = get_tweet_by_id(tweet_id)
        if _filters_satisfied(tweet, filters):
            serialized_tweet_item = json.dumps(jsonable_encoder(tweet))
            return serialized_tweet_item
        else:
            return None
    except (JSONDecodeError, KeyError):
        return None


def _filters_satisfied(tweet: TweetItem, filters: Dict[str, List[str]]) -> bool:
    """Check if the tweet satisfy the provided filters list"""
    for key, value in filters.items():
        if key == "keywords" and not _keywords_found(tweet, value):
            return False
        if key == "langs" and not _is_in_languages(tweet, value):
            return False
        if key == "usernames" and not _is_of_usernames(tweet, value):
            return False
        # NOTE: no need to check locations, because if present will be used as main filter
    return True


def _keywords_found(tweet: TweetItem, keywords: List[str]) -> bool:
    """Check if one of the keywords provided is found in the tweet text"""
    for keyword in keywords:
        if keyword in tweet.text:
            return True
    return False


def _is_in_languages(tweet: TweetItem, langs: List[str]) -> bool:
    """Check if the tweet has one of the specified languages"""
    for lang in langs:
        if tweet.lang == lang:
            return True
    return False


def _is_of_usernames(tweet: TweetItem, usernames: List[str]) -> bool:
    """Checks if the tweet has one of the listed authors"""
    for user in usernames:
        if TYPE_CHECKING:
            assert tweet.user
        if tweet.user.screen_name.lower()[1:] == user.lower():
            return True
    return False


def get_competition_stream_strategy(
    root_id: str,
) -> Callable[[str], Optional[str]]:
    """Compose the book competition strategy with the root_id; then return the strategy,
    ready to be fed to a ServerSideEventRelay.build_event_source_response"""
    return partial(book_competition_from_v1_data_strategy, root_id)


def book_competition_from_v1_data_strategy(
    root_id: str, tweet_data_str: str
) -> Optional[str]:
    """TODO"""
    try:
        tweet_data = json.loads(tweet_data_str)
        tweet_id = str(tweet_data["id_str"])
    except (JSONDecodeError, KeyError):
        return None
    tweet = get_tweet_by_id(tweet_id)

    if tweet.conversation_id == int(root_id):
        competition = get_book_competition(root_id)
        serialized_competition = json.dumps(jsonable_encoder(competition))
        return serialized_competition
    else:
        return None
