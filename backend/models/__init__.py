from pydantic.main import BaseModel

from .distribution import DistributionResults, TweetsDateCount
from .geo import TweetGeo, TweetBoundingBox, Coordinates, TweetPlace
from .media import TweetMedia
from .mentions import TweetMentions
from .metrics import TweetMetrics
from .referenced import TweetReferenced
from .user import TweetUser
from .tweet import TweetItem
from .search_results import SearchResults
from .sentiment_analysis import SAMetrics, SAResponse, SAQuery, SARequest, SAScored

# Explicitly export symbols from this module
__all__ = [
    "DistributionResults",
    "TweetsDateCount",
    "TweetGeo",
    "TweetBoundingBox",
    "Coordinates",
    "TweetPlace",
    "TweetMentions",
    "TweetMetrics",
    "TweetReferenced",
    "TweetUser",
    "TweetMedia",
    "TweetItem",
    "SearchResults",
    "SAMetrics",
    "SAResponse",
    "SAQuery",
    "SARequest",
    "SAScored",
    "Message",
]


class Message(BaseModel):
    message: str
