from __future__ import annotations

from typing import Optional, Tuple, List, Dict, Union
from pydantic import BaseModel

from tweepy import Place, Media, User, Tweet

from . import TweetGeo
from . import TweetMedia
from . import TweetMentions
from . import TweetMetrics
from . import TweetPlace
from . import TweetReferenced
from . import TweetUser


# Type alias for the includes dictionary from a v2.0 response
IncludesType = Dict[str, Union[User, Media, Place]]


class TweetItem(BaseModel):
    # the id of the tweet
    id_str: str
    # The tweet full text
    text: str
    # geo: used to hold coordinates when precise location is enabled
    geo: Optional[TweetGeo]
    # place: used to hold 'places', geolocation polygon to approssimate the location
    place: Optional[TweetPlace]
    # user: name and profile img
    user: Optional[TweetUser]
    # metrics: like, retweet, quote, reply
    metrics: Optional[TweetMetrics]
    # media: photo/video
    media: Optional[List[TweetMedia]]
    # when the tweet was created
    created_at: str
    # if there is a link that contain sensitive content
    possibly_sensitive: bool
    # id del tweet retwettato
    referenced_tweet: Optional[TweetReferenced]
    # mentioned users
    mentions: Optional[List[TweetMentions]]
    # list of hashtags
    hashtags: Optional[List[str]]
    # lista di links
    urls: Optional[List[str]]
    # lingua del tweet
    lang: Optional[str]
    # identificatore della conversazione
    conversation_id: int
    # risposte al tweet
    replies: Optional[List[TweetItem]]

    @staticmethod
    def from_data(data: Tweet, includes: IncludesType) -> TweetItem:
        """Create a TweetItem object starting from the Status object returned from tweepy."""

        referenced_tweet = TweetItem._parse_referenced_tweet_data(data)

        geo, place = TweetItem._parse_position_data(data, includes)

        user = TweetItem._parse_user_data(data, includes)

        media = TweetItem._parse_media_data(data, includes)

        metrics = TweetMetrics.from_metrics(data.public_metrics)

        hashtags, mentions, urls = TweetItem._parse_entities_data(data)

        lang = TweetItem._parse_lang_data(data)

        return TweetItem(
            id_str=data.data["id"],
            text=data.text,
            geo=geo,
            place=place,
            user=user,
            metrics=metrics,
            created_at=data.data["created_at"],
            possibly_sensitive=data.data["possibly_sensitive"],
            media=media,
            referenced_tweet=referenced_tweet,
            mentions=mentions,
            hashtags=hashtags,
            urls=urls,
            lang=lang,
            conversation_id=data.conversation_id,
            replies=None,
        )

    @staticmethod
    def _parse_lang_data(data: Tweet) -> Optional[str]:
        lang = None
        if data.lang is not None:
            lang = data.lang
        return lang

    @staticmethod
    def _parse_referenced_tweet_data(data: Tweet) -> Optional[TweetReferenced]:
        referenced_tweet = None

        if data.referenced_tweets is not None:
            referenced_tweet = TweetReferenced.from_retweet(data.referenced_tweets[0])

        return referenced_tweet

    @staticmethod
    def _parse_entities_data(
        data: Tweet,
    ) -> Tuple[Optional[List[str]], Optional[List[TweetMentions]], Optional[List[str]]]:
        urls = None
        hashtags = None
        mentions = None

        if data.entities is not None:
            mentions = TweetItem._parse_mentions_data(data)
            hashtags = TweetItem._parse_hashtags_data(data)
            urls = TweetItem._parse_urls_data(data)

        return hashtags, mentions, urls

    @staticmethod
    def _parse_urls_data(data: Tweet) -> Optional[List[str]]:
        urls = None

        if "urls" in data.entities:
            urls = []
            for url in data.entities["urls"]:
                urls.append(url["url"])

        return urls

    @staticmethod
    def _parse_hashtags_data(data: Tweet) -> Optional[List[str]]:
        hashtags = None

        if "hashtags" in data.entities:
            hashtags = []
            for hashtag in data.entities["hashtags"]:
                hashtags.append(hashtag["tag"])

        return hashtags

    @staticmethod
    def _parse_mentions_data(data: Tweet) -> Optional[List[TweetMentions]]:
        mentions = None

        if "mentions" in data.entities:
            mentions = []
            for mention in data.entities["mentions"]:
                mentions.append(TweetMentions.from_mentions(mention))

        return mentions

    @staticmethod
    def _parse_user_data(data: Tweet, includes: IncludesType) -> Optional[TweetUser]:
        user = None

        for user_data in includes["users"]:
            if user_data.id == data.author_id:
                user = TweetUser.from_user(user_data)

        return user

    @staticmethod
    def _parse_media_data(
        data: Tweet, includes: IncludesType
    ) -> Optional[List[TweetMedia]]:
        media = None

        if data.attachments is not None and "media_keys" in data.attachments:
            media = []
            for media_key in data.attachments["media_keys"]:
                for media_el in includes["media"]:
                    if media_el.media_key == media_key:
                        media.append(TweetMedia.from_media(media_el))
                        break

        return media

    @staticmethod
    def _parse_position_data(
        data: Tweet, includes: IncludesType
    ) -> Tuple[Optional[TweetGeo], Optional[TweetPlace]]:
        place = None
        geo = None

        if data.geo is not None:
            if "coordinates" in data.geo:
                geo = TweetGeo.from_dict(data.geo["coordinates"])

            if "place_id" in data.geo:
                for p in includes["places"]:
                    if p.id == data.geo["place_id"]:
                        place = TweetPlace.from_place(p)

        return geo, place


# Needed to allow forwardRefs with pydantic
TweetItem.update_forward_refs()
