from __future__ import annotations

from pydantic import BaseModel
from tweepy import ReferencedTweet


class TweetReferenced(BaseModel):
    id: int
    # 'retweet', 'quote', 'replied_to'
    type: str

    @staticmethod
    def from_retweet(retweet: ReferencedTweet) -> TweetReferenced:
        return TweetReferenced(
            id=retweet.id,
            type=retweet.type,
        )
