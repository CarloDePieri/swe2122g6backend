from __future__ import annotations

from typing import Dict, Union

from pydantic import BaseModel


class TweetMentions(BaseModel):
    username: str
    id: str

    @staticmethod
    def from_mentions(mention: Dict[str, Union[int, str]]) -> TweetMentions:
        return TweetMentions(username=mention["username"], id=mention["id"])
