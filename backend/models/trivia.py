from __future__ import annotations
from typing import List, Dict, TYPE_CHECKING, Optional, Tuple

from pydantic import BaseModel

from backend.models import TweetItem, TweetReferenced, TweetUser, SearchResults


class Place(BaseModel):
    avatar: str
    name: str
    points: int

    @staticmethod
    def from_user(user: TweetUser) -> Place:
        avatar = user.full_img
        name = user.name
        points = 0
        return Place(avatar=avatar, name=name, points=points)


class Answer(BaseModel):
    text: str
    # lista di screen_name che hanno risposto quest'opzione
    users: List[str]

    @staticmethod
    def from_risposta(option: str, tweet: TweetItem) -> Answer:
        users = []
        if tweet.replies is not None:
            if TYPE_CHECKING:
                assert type(tweet.user) is TweetUser
            for answer in tweet.replies:
                if TYPE_CHECKING:
                    assert type(answer.user) is TweetUser
                # ad ogni ciclo controllo se le risposte coincidono, se coincidono aggiungo lo screen_name alla lista
                if (
                    option[0].lower()
                    == Answer._parse_tweet_text(
                        answer.text, tweet.user.screen_name
                    ).lower()
                ):
                    users.append(answer.user.screen_name)
        # i primi tre caratteri di option corrispondono a A), B), C) o D)
        text = option[3:]
        return Answer(text=text, users=users)

    @staticmethod
    def _parse_tweet_text(reply_text: str, screen_name: str) -> str:
        """consent to edit replies text only once"""
        reply_text = reply_text.replace("#group6answer", "")
        reply_text = reply_text.replace(f"{screen_name}", "")
        reply_text = reply_text.replace(" ", "")
        return reply_text


class Question(BaseModel):
    tweet_id_str: str
    text: str
    answers: Dict[str, Answer]
    # solution è uguale ad una lettera (A, B, C, D) che corrisponde alla risposta giusta
    solution: Optional[str]

    @staticmethod
    def from_tweet(tweet: TweetItem, correct_answers: List[TweetItem]) -> Question:
        answers = {}

        text, choices = Question._parse_splitted_text(tweet.text)

        solution = Question._parse_correct_answer(correct_answers, tweet)

        for choice in choices:
            answers[choice[0]] = Answer.from_risposta(choice, tweet)
        return Question(
            tweet_id_str=tweet.id_str, text=text, answers=answers, solution=solution
        )

    @staticmethod
    def _parse_correct_answer(
        correct_answers: List[TweetItem], tweet: TweetItem
    ) -> Optional[str]:
        """if there is a solution with the same referenced_tweet, return it without hashtags"""
        solution = None
        for correct in correct_answers:
            if TYPE_CHECKING:
                assert type(correct.referenced_tweet) is TweetReferenced
            if str(correct.referenced_tweet.id) == tweet.id_str:
                solution = correct.text
                solution = solution.replace("#group6correctanswer ", "")
                # prendo solo la prima lettera della soluzione
                solution = solution[0].upper()
        return solution

    @staticmethod
    def _parse_splitted_text(text: str) -> Tuple[str, List[str]]:
        """split the question text from the choices text"""
        choices = text.replace("#group6newquestion ", "")
        # cerco dov'è il punto interrrogativo e lo interpreto come fine della domanda
        n = choices.rfind("?")
        text = choices[: n + 1]
        choices = choices[n + 2 :]
        # cerco tutti gli "a capo" e li interpreto come fine dell'opzione
        list_choices = choices.split("\n")
        return text, list_choices


class InvalidIdErrorTrivia(Exception):
    """Raise when the tweet isn't a Trivia"""


class Trivia(BaseModel):
    root_tweet: TweetItem
    trivia_tree: List[TweetItem]
    # lista di oggetti domanda che contengono il testo e le risposte
    questions: List[Question]
    # classifica che contiene screen_name come key e full_img, name e points come value
    placement: Dict[str, Place]

    @staticmethod
    def from_interaction(interaction: SearchResults, root_tweet: TweetItem) -> Trivia:
        questions = []
        if root_tweet.hashtags is None or "group6newtrivia" not in root_tweet.hashtags:
            raise InvalidIdErrorTrivia("this tweet is not a valid Trivia")

        # crea 3 array di domande, risposte e soluzioni
        interaction_tree, answers, correct_answers = Trivia._parse_interaction_data(
            interaction, root_tweet
        )

        # controllo che nessuno abbia risposto due volte alla stessa domanda
        filtered_answers = Trivia._get_valid_users_answers(answers)

        # aggiungo le risposte nelle replies, controllo se sono scadute prima
        Trivia._append_answers(interaction_tree, filtered_answers, correct_answers)

        for tweet in interaction_tree:
            questions.append(Question.from_tweet(tweet, correct_answers))

        placement = Trivia._parse_placement(filtered_answers, questions)

        return Trivia(
            root_tweet=root_tweet,
            trivia_tree=interaction_tree,
            questions=questions,
            placement=placement,
        )

    @staticmethod
    def _parse_placement(
        filtered_answers: List[TweetItem], questions: List[Question]
    ) -> Dict[str, Place]:
        """First I add al the partecipants in the classification, then I add points"""
        placement = {}
        for answer in filtered_answers:
            if TYPE_CHECKING:
                assert type(answer.user) is TweetUser
            if answer.user.screen_name not in placement:
                placement[answer.user.screen_name] = Place.from_user(answer.user)
        for question in questions:
            if question.solution is not None:
                list_user_corrects = question.answers[question.solution]
                for user in list_user_corrects.users:
                    placement[user].points = placement[user].points + 1
        return placement

    @staticmethod
    def _parse_interaction_data(
        interaction: SearchResults, root_tweet: TweetItem
    ) -> Tuple[List[TweetItem], List[TweetItem], List[TweetItem]]:
        interaction_tree = []
        answers = []
        correct_answers = []
        if TYPE_CHECKING:
            assert type(root_tweet.user) is TweetUser
        for tweet in interaction.results:
            if tweet.hashtags is not None:
                if TYPE_CHECKING:
                    assert type(tweet.referenced_tweet) is TweetReferenced
                    assert type(tweet.user) is TweetUser
                if (
                    "group6answer" in tweet.hashtags
                    and tweet.user.screen_name != root_tweet.user.screen_name
                ):
                    answers.append(tweet)
                elif (
                    "group6newquestion" in tweet.hashtags
                    and tweet.referenced_tweet.id == int(root_tweet.id_str)
                ):
                    interaction_tree.append(tweet)
                elif (
                    tweet.user.screen_name == root_tweet.user.screen_name
                    and "group6correctanswer" in tweet.hashtags
                ):
                    correct_answers.append(tweet)
        return interaction_tree, answers, correct_answers

    @staticmethod
    def _get_valid_users_answers(answers: List[TweetItem]) -> List[TweetItem]:
        """Return a list of tweet with valid votes."""
        filtered = {}
        # we need answers in the reverse order to count older votes first
        for answer in reversed(answers):
            if TYPE_CHECKING:
                assert type(answer.user) is TweetUser
            key = answer.user.screen_name
            if key not in filtered:
                filtered[key] = [answer]
            else:
                already_answered = False
                already_answered = Trivia._is_duplicated(
                    already_answered, answer, filtered, key
                )

                if not already_answered:
                    filtered[key].append(answer)

        # we only need a list of valid votes, let's build it from the filtered dict
        user_answers = [
            answer for user_answers in [*filtered.values()] for answer in user_answers
        ]
        return list(reversed(user_answers))

    @staticmethod
    def _is_duplicated(
        already_answered: bool,
        answer: TweetItem,
        filtered: Dict[str, List[TweetItem]],
        key: str,
    ) -> bool:
        if TYPE_CHECKING:
            assert type(answer.referenced_tweet) is TweetReferenced
        for accepted_answer in filtered[key]:
            if TYPE_CHECKING:
                assert type(accepted_answer.referenced_tweet) is TweetReferenced
            if answer.referenced_tweet.id == accepted_answer.referenced_tweet.id:
                already_answered = True
                break
        return already_answered

    @staticmethod
    def _append_answers(
        interaction_tree: List[TweetItem],
        filtered_answers: List[TweetItem],
        correct_answers: List[TweetItem],
    ) -> None:
        for answer in filtered_answers:
            for question in interaction_tree:
                expiration = None
                Trivia.check_answer_and_expiration(
                    answer, correct_answers, expiration, question
                )

    @staticmethod
    def check_answer_and_expiration(
        answer: TweetItem,
        correct_answers: list[TweetItem],
        expiration: Optional[str],
        question: TweetItem,
    ) -> None:
        for correct in correct_answers:
            if TYPE_CHECKING:
                assert type(correct.referenced_tweet) is TweetReferenced
            if str(correct.referenced_tweet.id) == question.id_str:
                expiration = correct.created_at
            if TYPE_CHECKING:
                assert type(answer.referenced_tweet) is TweetReferenced
            if int(question.id_str) == answer.referenced_tweet.id and (
                expiration is None or expiration > answer.created_at
            ):
                if question.replies is None:
                    question.replies = [answer]
                else:
                    question.replies.append(answer)
                break
