from __future__ import annotations

from pydantic import BaseModel
from tweepy import Media


class TweetMedia(BaseModel):
    type: str
    url: str

    @staticmethod
    def from_media(media: Media) -> TweetMedia:
        if "url" in media.data:
            url = media.data["url"]
        else:
            url = media.data["preview_image_url"]
        return TweetMedia(
            type=media.data["type"],
            url=url,
        )
