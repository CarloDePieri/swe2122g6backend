from __future__ import annotations

from typing import Dict

from pydantic import BaseModel


class TweetMetrics(BaseModel):
    retweet: int
    reply: int
    like: int
    quote: int

    @staticmethod
    def from_metrics(metrics: Dict[str, int]) -> TweetMetrics:
        return TweetMetrics(
            retweet=metrics["retweet_count"],
            reply=metrics["reply_count"],
            like=metrics["like_count"],
            quote=metrics["quote_count"],
        )
