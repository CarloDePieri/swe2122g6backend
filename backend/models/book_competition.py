from __future__ import annotations

from typing import List, Dict, TYPE_CHECKING, Tuple
from pydantic import BaseModel

from backend.models import TweetItem, SearchResults

if TYPE_CHECKING:
    from backend.models import TweetUser, TweetReferenced

from datetime import datetime

from dateutil.relativedelta import relativedelta


class InvalidCompetitionIdError(Exception):
    """Raise when the tweet isn't a book competition"""


class BookCompetition(BaseModel):

    root_tweet: TweetItem
    conversation_tree: List[TweetItem]
    books: Dict[str, int]

    @staticmethod
    def from_conversation(
        conversation: SearchResults, root_tweet: TweetItem
    ) -> BookCompetition:

        BookCompetition.check_valid_competition(root_tweet)

        root_id = root_tweet.id_str

        expiration_date = BookCompetition._create_expiration_date(root_tweet.created_at)

        conversation_tree, votes = BookCompetition._parse_conversation_data(
            conversation, root_id, expiration_date
        )

        filtered_votes = BookCompetition._get_valid_users_votes(votes)

        BookCompetition._append_votes(conversation_tree, filtered_votes)

        books = BookCompetition._create_books_dict(conversation_tree, root_tweet)

        return BookCompetition(
            root_tweet=root_tweet, conversation_tree=conversation_tree, books=books
        )

    @staticmethod
    def check_valid_competition(root_tweet: TweetItem) -> None:
        if (
            root_tweet.hashtags is None
            or "group6newcompetition" not in root_tweet.hashtags
        ):
            raise InvalidCompetitionIdError(
                "this tweet is not a valid book competition"
            )

    @staticmethod
    def _create_expiration_date(root_date: str) -> str:
        date_time_obj = datetime.strptime(root_date, "%Y-%m-%dT%H:%M:%S.%fZ")
        new_date = date_time_obj + relativedelta(months=2)
        new_date_str = new_date.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        return new_date_str

    @staticmethod
    def _create_books_dict(
        conversation_tree: List[TweetItem], root_tweet: TweetItem
    ) -> Dict[str, int]:
        books = {}
        for book in conversation_tree:
            key = book.text
            if TYPE_CHECKING:
                assert type(root_tweet.user) is TweetUser
                assert type(root_tweet.hashtags) is List[str]
            key = key.replace(f"{root_tweet.user.screen_name} ", "")
            key = key.replace("#group6submitbook ", "")
            for hashtag in root_tweet.hashtags:
                key = key.replace(f"#{hashtag} ", "")
            if book.replies is not None:
                books[key] = len(book.replies)
            else:
                books[key] = 0
        return books

    @staticmethod
    def _append_votes(
        conversation_tree: List[TweetItem], filtered_votes: List[TweetItem]
    ) -> None:
        for vote in filtered_votes:
            for book in conversation_tree:
                if TYPE_CHECKING:
                    assert type(vote.referenced_tweet) is TweetReferenced
                if int(book.id_str) == vote.referenced_tweet.id:
                    if book.replies is None:
                        book.replies = [vote]
                    else:
                        book.replies.append(vote)
                    break

    @staticmethod
    def _get_valid_users_votes(votes: List[TweetItem]) -> List[TweetItem]:
        """Return a list of tweet with valid votes."""
        filtered = {}
        # we need votes in the reverse order to count older votes first
        for vote in reversed(votes):
            if TYPE_CHECKING:
                assert type(vote.user) is TweetUser
            user_name = vote.user.screen_name
            if user_name not in filtered:
                filtered[user_name] = [vote]
            else:
                # we don't want duplicate votes
                duplicated_vote = BookCompetition._is_duplicated_vote(
                    vote, filtered[user_name]
                )
                # 10 votes max!
                if len(filtered[user_name]) <= 10 and not duplicated_vote:
                    filtered[user_name].append(vote)

        # we only need a list of valid votes, let's build it from the filtered dict
        user_votes = [
            vote for user_votes in [*filtered.values()] for vote in user_votes
        ]
        return list(reversed(user_votes))

    @staticmethod
    def _is_duplicated_vote(vote: TweetItem, accepted_votes: List[TweetItem]) -> bool:
        if TYPE_CHECKING:
            assert type(vote.referenced_tweet) is TweetReferenced
        for accepted_vote in accepted_votes:
            if TYPE_CHECKING:
                assert type(accepted_vote.referenced_tweet) is TweetReferenced
            if vote.referenced_tweet.id == accepted_vote.referenced_tweet.id:
                return True
        return False

    @staticmethod
    def _parse_conversation_data(
        conversation: SearchResults, root_id: str, expiration_date: str
    ) -> Tuple[List[TweetItem], List[TweetItem]]:
        conversation_tree = []
        votes = []
        for tweet in conversation.results:
            if tweet.hashtags is not None and tweet.created_at <= expiration_date:
                if TYPE_CHECKING:
                    assert type(tweet.referenced_tweet) is TweetReferenced
                if "group6votebook" in tweet.hashtags:
                    votes.append(tweet)
                elif (
                    "group6submitbook" in tweet.hashtags
                    and tweet.referenced_tweet.id == int(root_id)
                ):
                    conversation_tree.append(tweet)
        return conversation_tree, votes
