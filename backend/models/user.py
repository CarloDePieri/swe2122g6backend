from __future__ import annotations

from pydantic import BaseModel
from tweepy import User


class TweetUser(BaseModel):
    name: str
    img: str
    full_img: str
    id: int
    # eg: @myusername
    screen_name: str

    @staticmethod
    def from_user(user: User) -> TweetUser:
        username = user.username
        return TweetUser(
            name=user.name,
            img=user.profile_image_url,
            full_img=user.profile_image_url.replace("_normal", ""),
            id=user.id,
            screen_name=f"@{username}",
        )
