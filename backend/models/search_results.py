from __future__ import annotations

from typing import List

from pydantic import BaseModel
from tweepy import Response

from . import TweetItem, DistributionResults


class SearchResults(BaseModel):
    results: List[TweetItem]
    distribution_results: DistributionResults

    @staticmethod
    def from_responses(
        results_response: Response, distribution_response: Response
    ) -> SearchResults:
        tweet_includes = results_response.includes
        sr = SearchResults(
            results=[],
            distribution_results=DistributionResults.from_response(
                distribution_response
            ),
        )
        if results_response.data is not None:
            for tweet_data in results_response.data:
                tweet_item = TweetItem.from_data(tweet_data, tweet_includes)
                sr.results.append(tweet_item)
        return sr
