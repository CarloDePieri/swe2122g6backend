from __future__ import annotations

from typing import List, Dict, Union

from pydantic import BaseModel
from tweepy import Response


class TweetsDateCount(BaseModel):
    start: str
    end: str
    count: int

    @staticmethod
    def from_dict(data: Dict[str, Union[str, int]]) -> TweetsDateCount:
        return TweetsDateCount(
            start=data["start"], end=data["end"], count=data["tweet_count"]
        )


class DistributionResults(BaseModel):
    total_tweet_count: int
    distribution: List[TweetsDateCount]

    @staticmethod
    def from_response(resp: Response) -> DistributionResults:
        distribution = [TweetsDateCount.from_dict(data) for data in resp.data[1:]]
        return DistributionResults(
            distribution=distribution,
            total_tweet_count=resp.meta["total_tweet_count"],
        )
