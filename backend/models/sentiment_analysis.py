from __future__ import annotations

from typing import List, Callable

from pydantic import BaseModel

from backend.sentiment import get_score


class SAMetrics(BaseModel):
    total_tweets_count: int
    positive_tweets_count: int
    negative_tweets_count: int
    neutral_tweets_count: int

    @staticmethod
    def from_scored(scored: List[SAScored]) -> SAMetrics:
        def count_filtered_score(f: Callable[[SAScored], bool]) -> int:
            return len(list(filter(f, scored)))

        return SAMetrics(
            total_tweets_count=len(scored),
            positive_tweets_count=count_filtered_score(lambda x: x.score > 0),
            negative_tweets_count=count_filtered_score(lambda x: x.score < 0),
            neutral_tweets_count=count_filtered_score(lambda x: x.score == 0),
        )


class SAQuery(BaseModel):
    text: str
    lang: str


class SARequest(BaseModel):
    to_score: List[SAQuery]


class SAScored(SAQuery):
    score: float

    @staticmethod
    def from_query(query: SAQuery) -> SAScored:
        score = get_score(query.text, query.lang)
        return SAScored(text=query.text, lang=query.lang, score=score)


class SAResponse(BaseModel):
    scored: List[SAScored]
    metrics: SAMetrics

    @staticmethod
    def from_request(request: SARequest) -> SAResponse:
        scored = list(map(lambda x: SAScored.from_query(x), request.to_score))
        return SAResponse(scored=scored, metrics=SAMetrics.from_scored(scored))
