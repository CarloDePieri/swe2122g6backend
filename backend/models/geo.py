from __future__ import annotations

from typing import List

from pydantic import BaseModel
from tweepy import Tweet, Place


class Coordinates(BaseModel):
    lat: float
    lon: float

    @staticmethod
    def from_list(float_list: List[float]) -> Coordinates:
        return Coordinates(lat=float_list[1], lon=float_list[0])


class TweetBoundingBox(BaseModel):
    # eg: [12.00, 23.44, 12.00, 23.44]
    bbox: List[float]
    center: Coordinates

    @staticmethod
    def from_bb(bbox: List[float]) -> TweetBoundingBox:
        bbox_list = [bbox[0], bbox[1], bbox[2], bbox[3]]
        center_list = [
            (bbox[0] + bbox[2]) / 2,
            (bbox[1] + bbox[3]) / 2,
        ]
        return TweetBoundingBox(
            bbox=bbox_list, center=Coordinates.from_list(center_list)
        )


class TweetGeo(BaseModel):
    coordinates: Coordinates
    # eg: Point
    type: str

    @staticmethod
    def from_dict(data: Tweet) -> TweetGeo:
        coordinates = data["coordinates"]
        assert isinstance(
            coordinates, list
        )  # used to signal mypy that data["coordinates"] is indeed a list
        return TweetGeo(
            type=data["type"],
            coordinates=Coordinates.from_list(coordinates),
        )


class TweetPlace(BaseModel):
    country: str
    country_code: str
    full_name: str
    id: str
    name: str
    place_type: str
    bounding_box: TweetBoundingBox

    @staticmethod
    def from_place(place: Place) -> TweetPlace:
        bbox = place.data["geo"]["bbox"]
        assert isinstance(bbox, list)
        bb = TweetBoundingBox.from_bb(bbox)
        return TweetPlace(
            bounding_box=bb,
            country=place.country,
            country_code=place.country_code,
            full_name=place.full_name,
            id=place.id,
            name=place.name,
            place_type=place.place_type,
        )
