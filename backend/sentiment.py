from googletrans import Translator  # type: ignore
from afinn import Afinn  # type: ignore


AFINN_LANG = ["da", "en", "emoticons", "fi", "fr", "it", "pl", "sv", "tr"]


def get_score(query: str, lang: str) -> float:

    replacement = [
        ["#", ""],
        ["_", " "],
        ["-", " "],
        ["@", ""],
    ]

    for r in replacement:
        query = query.replace(r[0], r[1])

    if lang in AFINN_LANG:
        afinn = Afinn(language=lang)
        score = afinn.score(query)  # type: float

        if score == 0.0:
            score = _translate_and_score(query, lang)
    else:
        score = _translate_and_score(query, lang)

    return score


def _translate_and_score(query: str, lang: str) -> float:
    afinn = Afinn()
    try:
        query = _translate_to_english(query, lang)
    except ValueError:
        return 0
    score = afinn.score(query)  # type: float
    return score


def _translate_to_english(query: str, lang: str) -> str:
    if lang == "und":
        translated = Translator().translate(text=query, dest="en")
    else:
        translated = Translator().translate(text=query, dest="en", src=lang)
    text = translated.text  # type: str
    return text
