import os
import base64


class Secrets:
    """Secrets recovers and holds all secret values needed by the backend."""

    bearer_token: str
    access_token: str
    access_token_secret: str
    key: str
    key_secret: str
    secondary_bearer_token: str
    secondary_access_token: str
    secondary_access_token_secret: str
    secondary_key: str
    secondary_key_secret: str

    def __init__(self) -> None:
        """Look for the secret in the environment first, then in the secrets file."""
        secrets = [
            "BEARER_TOKEN",
            "ACCESS_TOKEN",
            "ACCESS_TOKEN_SECRET",
            "KEY",
            "KEY_SECRET",
            "SECONDARY_BEARER_TOKEN",
            "SECONDARY_ACCESS_TOKEN",
            "SECONDARY_ACCESS_TOKEN_SECRET",
            "SECONDARY_KEY",
            "SECONDARY_KEY_SECRET",
        ]
        for name in secrets:
            # Read the secret from the environment
            secret = os.getenv(name)
            if secret is None:
                try:
                    # If the secret were not in the env, try to get it from the secret file
                    import backend.secret_values

                    secret = getattr(backend.secret_values, name)
                except ModuleNotFoundError:
                    # If the file is not there, raise an error
                    raise SecretNotFoundException(
                        f"Could not find secret {name} . Either have an environment variable set or put the secret in"
                        " backend/secret_values.py (take a look at backend/secret_values.template.py for hints)."
                    )

            # Decode and assign the secret value to this object respective field
            decoded_secret = self.decode(secret)
            setattr(self, name.lower(), decoded_secret)

    @staticmethod
    def decode(encoded_string: str) -> str:
        encoded_bytes_string = encoded_string.encode("utf-8")
        decoded_bytes_string = base64.b64decode(encoded_bytes_string)
        return decoded_bytes_string.decode("utf-8")


class SecretNotFoundException(Exception):
    """Exception used to signal the user a secret value is missing."""
