import re

import tweepy
from tweepy import Client, Response

from backend.models import (
    SearchResults,
    SARequest,
    SAResponse,
    TweetItem,
)
from backend.models.trivia import Trivia
from backend.models.book_competition import BookCompetition
from backend.secrets import Secrets


class UserLookupError(Exception):
    """Raised when an error was raised when looking for a user."""


class ApiWrapper:
    """TODO"""

    client: Client

    tweet_data_model = {
        "tweet_fields": [
            "id",
            "author_id",
            "geo",
            "text",
            "created_at",
            "attachments",
            "possibly_sensitive",
            "public_metrics",
            "referenced_tweets",
            "entities",
            "lang",
            "conversation_id",
        ],
        "expansions": ["geo.place_id", "author_id", "attachments.media_keys"],
        "place_fields": [
            "geo",
            "country",
            "country_code",
            "place_type",
            "name",
        ],
        "user_fields": [
            "profile_image_url",
            "entities",
        ],
        "media_fields": [
            "preview_image_url",
            "url",
        ],
    }

    def __init__(self) -> None:
        """Constructor for ApiWrapper"""
        secrets = Secrets()

        self.client = tweepy.Client(
            bearer_token=secrets.bearer_token,
            access_token=secrets.access_token,
            access_token_secret=secrets.access_token_secret,
            consumer_key=secrets.key,
            consumer_secret=secrets.key_secret,
        )

    def search_all(self, query: str, limit: int) -> Response:
        self._check_query_for_username(query)
        return self.client.search_all_tweets(
            query=query,
            max_results=limit,
            start_time="2021-01-01T00:00:00Z",
            **self.tweet_data_model,
        )

    def get_tweets_distribution(self, query: str) -> Response:
        """TODO"""
        self._check_query_for_username(query)
        return self.client.get_all_tweets_count(query=query, granularity="day")

    def get_username_id(self, screen_name: str) -> int:
        user_response = self.client.get_user(username=screen_name)
        if len(user_response.errors) > 0:
            error = user_response.errors[0]
            raise UserLookupError(f"{error['title']} - {error['detail']}")
        assert isinstance(user_response.data.id, int)  # mypy check
        return user_response.data.id

    def _check_query_for_username(self, query: str) -> None:
        matches = re.finditer(r"from:(\w*)", query, re.MULTILINE)
        for match in matches:
            # This will also check for user errors
            self.get_username_id(screen_name=match.group(1))

    def get_tweet_by_id(self, id_str: str) -> Response:
        tweet = self.client.get_tweet(id=id_str, **self.tweet_data_model)
        if len(tweet.errors) > 0:
            raise InvalidTweetIdException(tweet.errors[0]["detail"])
        else:
            return tweet


class InvalidTweetIdException(Exception):
    """Exception when an id does not exist"""


def search_all(query: str, limit: int) -> SearchResults:
    api_wrapper = ApiWrapper()
    results_response = api_wrapper.search_all(query=query, limit=limit)
    distribution_response = api_wrapper.get_tweets_distribution(query=query)
    return SearchResults.from_responses(results_response, distribution_response)


def get_sentiment_analysis(query: SARequest) -> SAResponse:
    return SAResponse.from_request(query)


def get_tweet_by_id(id_str: str) -> TweetItem:
    wrapper = ApiWrapper()
    tweet_response = wrapper.get_tweet_by_id(id_str=id_str)
    return TweetItem.from_data(tweet_response.data, tweet_response.includes)


def get_book_competition(id_str: str) -> BookCompetition:
    root_tweet = get_tweet_by_id(id_str)
    query = f"conversation_id:{root_tweet.conversation_id}"
    conversation = search_all(query, limit=10)
    conversation_tree = BookCompetition.from_conversation(conversation, root_tweet)
    return conversation_tree


def get_trivia(id_str: str) -> Trivia:
    root_tweet = get_tweet_by_id(id_str)
    query = f"conversation_id:{root_tweet.conversation_id}"
    interaction = search_all(query, limit=30)
    trivia_data = Trivia.from_interaction(interaction, root_tweet)
    return trivia_data
