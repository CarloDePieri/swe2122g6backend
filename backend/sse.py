import asyncio
import logging
from typing import Callable, Optional, Dict, AsyncGenerator, Generator, Iterator

from fastapi import Request
from requests import Response
from sse_starlette.sse import EventSourceResponse

log2 = logging.getLogger("uvicorn.error")


class ServerSideEventRelay:
    """Relay data from a stream HTTP Response to an EventSourceResponse that can be returned from a FastAPI endpoint

    Parameters
    ----------
    stream_response : request.Response
        The stream response that will be the data origin
    api_request : fastapi.Request
        The api request that will govern this SSE. Will be used to monitor client disconnections
    """

    def __init__(self, stream_response: Response, api_request: Request):
        self.iterator_response = stream_response
        self.api_request = api_request

    def build_event_source_response(
        self, strategy: Optional[Callable[[str], Optional[str]]] = None
    ) -> EventSourceResponse:
        """Build the EventSourceResponse

        Parameters
        ----------
        strategy : Optional[Callable[[str], str]]
            A function that can optionally change the data that traverse the stream

        Returns
        ----------
        An EventSourceResponse that can then be returned from the api endpoint
        """

        async def event_publisher() -> AsyncGenerator[Dict[str, str], None]:
            iterator = self.iterator_response.iter_lines()
            try:
                log2.info("[Stream Started] Source connected")
                while True:
                    disconnected = await self.api_request.is_disconnected()
                    if disconnected:
                        break

                    data = self._pick_next(iterator, strategy)

                    yield {"data": data}

                # if it reaches this line the client was disconnected
                log2.info("[Stream Stopped] Api client disconnected")
                # Make extra sure the connection with Twitter get closed
                self.iterator_response.close()
            except asyncio.CancelledError as e:
                # this could trigger if the connection with the source http connection drops
                log2.info("[Stream Stopped] Error: source disconnected")
                raise e
            except StopIteration:
                log2.info("[Stream Stopped] No more data on the source stream")

        return EventSourceResponse(event_publisher())

    @staticmethod
    def _pick_next(
        iterator: Iterator[bytes],
        strategy: Optional[Callable[[str], Optional[str]]],
    ) -> str:
        """Return the next string in the stream.

        If given a strategy, keep iterating the stream until a suitable string is received and return that one"""
        while True:
            data = iterator.__next__().decode("utf-8")
            if strategy:
                parsed_data = strategy(data)
                if parsed_data is not None:
                    log2.debug(f"Stream data parsed: {parsed_data}")
                    return parsed_data
                else:
                    log2.debug(f"Stream data discarded: {parsed_data}")
            else:
                log2.debug(f"Stream data received: {data}")
                return data
