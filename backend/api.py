from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware
from sse_starlette.sse import EventSourceResponse
from tweepy import BadRequest
from typing import Union, Optional, List, Dict

from backend.models import (
    SearchResults,
    Message,
    SARequest,
    SAResponse,
    TweetItem,
)
from backend.models.book_competition import BookCompetition
from backend.models.trivia import Trivia, InvalidIdErrorTrivia
from backend.sse import ServerSideEventRelay
from backend.stream import (
    FilteredRawV1Stream,
    get_v2_tweet_from_v1_data_strategy,
    CannotGetStreamException,
    get_competition_stream_strategy,
)
from backend.twitter import (
    search_all,
    UserLookupError,
    get_sentiment_analysis,
    get_book_competition,
    get_trivia,
    get_tweet_by_id,
    InvalidTweetIdException,
)

from backend.models.book_competition import InvalidCompetitionIdError

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:3000",
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


user_404_response = {
    "model": Message,
    "description": "Not Found  -  Element was not available. Check the message for more information.",
}


@app.get(
    "/api/search",
    response_model=SearchResults,
    responses={400: {"model": Message}, 404: user_404_response},
)
async def search_tweets(
    q: str = "",
    u: str = "",
    p: str = "",
    bbox: str = "",
    limit: int = 10,
) -> Union[SearchResults, JSONResponse]:
    if p != "":
        q = f'{q} place:"{p}"'
    if u != "":
        q = f"{q} from:{u}"
    if bbox != "":
        bbox = bbox.replace(",", " ")
        q = f"{q} bounding_box: [{bbox}]"
    try:
        return search_all(query=q, limit=limit)
    except BadRequest as e:
        return JSONResponse(status_code=400, content={"message": str(e)})
    except UserLookupError as e:
        return JSONResponse(status_code=404, content={"message": str(e)})


@app.post("/api/sentiment", response_model=SAResponse)
async def sentiment(data: SARequest) -> SAResponse:
    return get_sentiment_analysis(data)


@app.get("/api/competition", response_model=BookCompetition)
async def search_competition(id: str = "") -> Union[BookCompetition, JSONResponse]:
    try:
        return get_book_competition(id)
    except InvalidCompetitionIdError as e:
        return JSONResponse(status_code=400, content={"message": str(e)})
    except InvalidTweetIdException as e:
        return JSONResponse(status_code=404, content={"message": str(e)})


@app.get("/api/trivia", response_model=Trivia)
async def search_trivia(id: str = "") -> Union[Trivia, JSONResponse]:
    try:
        return get_trivia(id)
    except InvalidIdErrorTrivia as e:
        return JSONResponse(status_code=400, content={"message": str(e)})
    except InvalidTweetIdException as e:
        return JSONResponse(status_code=404, content={"message": str(e)})


@app.get("/api/stream", response_model=TweetItem)
async def get_tweets_stream(
    request: Request,
    keywords: Optional[str] = None,
    usernames: Optional[str] = None,
    locs: Optional[str] = None,
    langs: Optional[str] = None,
) -> Union[EventSourceResponse, JSONResponse]:
    try:
        fs = FilteredRawV1Stream()

        # parse the given filters
        filters = _parse_filters(keywords, langs, locs, usernames)

        # Check if at least a filter has been specified
        if not (
            "locations" in filters
            or "follow_usernames" in filters
            or "track_keywords" in filters
        ):
            return JSONResponse(
                status_code=400,
                content={
                    "message": "Missing filter: choose one between 'keywords', 'locs', 'langs' and 'usernames'"
                },
            )

        # Set the stream rule with to the most relevant filter and remove it from the filters dict
        if filters.get("locations") is not None:
            loc = list(map(lambda x: float(x), filters["locations"]))
            fs.set_rules(locations=loc)
            del filters["locations"]
        elif filters.get("follow_usernames") is not None:
            fs.set_rules(follow_usernames=filters["follow_usernames"])
            del filters["follow_usernames"]
        else:
            fs.set_rules(track_keywords=filters["track_keywords"])
            del filters["track_keywords"]

        iterator_response = fs.get_stream_response()
        sse = ServerSideEventRelay(iterator_response, request)
        strategy = get_v2_tweet_from_v1_data_strategy(filters)

        return sse.build_event_source_response(strategy=strategy)
    except CannotGetStreamException as e:
        return JSONResponse(
            status_code=400,
            content={"message": f"The stream by Twitter could not be obtained: {e}"},
        )
    except BadRequest as e:
        return JSONResponse(
            status_code=400,
            content={"message": str(e)},
        )


def _parse_filters(
    keywords: Optional[str],
    langs: Optional[str],
    locs: Optional[str],
    usernames: Optional[str],
) -> Dict[str, List[str]]:
    """Return a dict with the provided, not None filters, turned into list of string"""
    filters: Dict[str, List[str]] = {}
    if keywords is not None:
        filters["track_keywords"] = keywords.split(",")
    if usernames is not None:
        filters["follow_usernames"] = usernames.split(",")
    if locs is not None:
        filters["locations"] = locs.split(",")
    if langs is not None:
        filters["languages"] = langs.split(",")
    return filters


@app.get("/api/competition_stream", response_model=BookCompetition)
async def get_competition_stream(
    request: Request, id: str = ""
) -> Union[EventSourceResponse, JSONResponse]:
    try:
        fs = FilteredRawV1Stream()
        fs.set_rules(track_keywords=["#group6votebook"])

        root_tweet = get_tweet_by_id(id)
        # This will trigger a check on the validity of the competition root
        BookCompetition.check_valid_competition(root_tweet)

        iterator_response = fs.get_stream_response()
        sse = ServerSideEventRelay(iterator_response, request)

        strategy = get_competition_stream_strategy(root_tweet.id_str)

        return sse.build_event_source_response(strategy=strategy)
    # TODO check for wrong tweet id
    except InvalidCompetitionIdError as e:
        return JSONResponse(status_code=400, content={"message": str(e)})
    except CannotGetStreamException as e:
        return JSONResponse(
            status_code=400,
            content={"message": f"The stream by Twitter could not be obtained: {e}"},
        )
