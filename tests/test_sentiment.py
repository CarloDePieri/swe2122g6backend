from backend.sentiment import get_score

from tests.conftest import vcr_record, vcr_delete_on_fail


@vcr_record
@vcr_delete_on_fail
class TestASentimentAnalysisModule:
    """Test: A sentiment analysis module..."""

    def test_should_correctly_score_a_positive_phrase(self):
        """A sentiment analysis module should correctly score a positive phrase."""
        text = "I love you"
        lang = "en"
        score = get_score(text, lang)
        assert score > 0

    def test_should_correctly_score_a_negative_phrase(self):
        """A sentiment analysis module should correctly score a negative phrase."""
        text = "I hate you"
        lang = "en"
        score = get_score(text, lang)
        assert score < 0

    def test_should_handle_a_phrase_in_different_language_than_english(self):
        """A sentiment analysis module should handle a phrase in different language than english."""
        text = "l'amore e' bello!"
        lang = "it"
        score = get_score(text, lang)
        assert score > 0

    def test_should_defer_to_translated_sentences_for_unsupported_language(self):
        """A sentiment analysis module should defer to translated sentences for unsupported language."""
        text = "Te quiero"
        lang = "es"
        score = get_score(text, lang)
        assert score > 0

    def test_should_try_to_remove_symbols(self):
        """A sentiment analysis module should try to remove symbols."""
        text = (
            "RT @antondepierro: #novax #vaccino #vaccinatevi_e_basta #vaccinoCovid #NoGreenPass #Amici21 "
            "#Covid #COVID19 #Covid_19 #amore"
        )
        lang = "und"
        score = get_score(text, lang)
        assert score > 0

    def test_should_handle_an_undetermined_language(self):
        """A sentiment analysis module should handle an undetermined language."""
        text = (
            "RT @antondepierro: #novax #vaccino #vaccinatevi_e_basta #vaccinoCovid #NoGreenPass #Amici21 "
            "#Covid #COVID19 #Covid_19 #amore"
        )
        lang = "und"
        score = get_score(text, lang)
        assert score > 0

    def test_should_try_the_english_translation_if_a_supported_language_score_is_zero(
        self,
    ):
        """A sentiment analysis module should try the english translation if a supported language score is zero."""
        text = "ti amo!"
        lang = "it"
        score = get_score(text, lang)
        assert score > 0

    def test_should_handle_unknown_language_code(self):
        """A sentiment analysis module should handle unknown language code."""
        text = "gibberish"
        lang = "in"  # sometimes tweets lang is set to 'in', for some reason
        score = get_score(text, lang)
        assert score == 0
