import json
from unittest.mock import patch

import responses
from fastapi.testclient import TestClient

from backend.api import app
from backend.models.book_competition import InvalidCompetitionIdError
from backend.stream import CannotGetStreamException
from tests.stream_utils import (
    truncate_twitter_stream_in_our_sse_relay,
)
from tests.conftest import vcr_record, vcr_delete_on_fail, spy_method

client = TestClient(app)


@vcr_record
@vcr_delete_on_fail
class TestTheSearchApi:
    """Test: the search api..."""

    def test_should_return_a_list_of_tweets(self):
        """The search api should return a list of tweets."""
        limit = 200
        response = client.get(f"/api/search/?q=newyork&limit={limit}")
        assert response.status_code == 200
        assert "results" in response.json()
        assert len(response.json()["results"]) <= limit
        if len(response.json()["results"]) > 0:
            tweet = response.json()["results"][0]
            assert "text" in tweet
            assert "screen_name" in tweet["user"]

    def test_should_raise_an_error_with_a_wrong_limit_parameter(self):
        """The search api should raise an error with a wrong limit parameter."""
        response = client.get("/api/search/?q=newyork&limit=ammaccabanane")
        assert response.status_code == 422

    def test_should_default_to_ten_elements_without_a_limit_parameter(self):
        """The search api should default to ten elements without a limit parameter."""
        response = client.get("/api/search/?q=newyork")
        assert response.status_code == 200
        assert len(response.json()["results"]) <= 10

    def test_should_support_encoded_special_characters(self):
        """The search api should support encoded special characters."""
        response = client.get("/api/search?q=%23newyork")
        assert response.status_code == 200
        assert len(response.json()["results"]) > 0

    def test_should_return_a_user_tweets(self):
        """A search endpoint with parameter u not None should return a user tweets."""
        response = client.get("/api/search/?u=elonmusk")
        assert response.status_code == 200
        results = response.json()["results"]
        assert len(results) > 0
        assert results[0]["user"]["screen_name"] == "@elonmusk"

    def test_should_be_able_to_handle_an_invalid_username(self):
        """A search endpoint should be able to handle an invalid username."""
        response = client.get(
            "/api/search/?u=askjhasgf234nj2n5k2b6j4v23k4j5h2g3k45jv2h43gv2k"
        )
        assert response.status_code == 400
        assert "`username` query parameter" in response.json()["message"]
        assert "does not match ^" in response.json()["message"]

    def test_should_handle_a_non_existing_username(self):
        """A search endpoint should handle a non existing username."""
        response = client.get("/api/search/?u=CarloDeBBLoL")
        assert response.status_code == 404
        assert (
            "Not Found Error - Could not find user with username: [CarloDeBBLoL]."
            in response.json()["message"]
        )

    def test_should_handle_disabled_users(self):
        """A search endpoint should handle disabled users."""
        response = client.get("/api/search/?u=newyork")
        assert response.status_code == 404
        assert (
            "Forbidden - User has been suspended: [newyork]."
            in response.json()["message"]
        )

    def test_with_parameter_b_not_none_should_return_a_bbox_tweet(self):
        """The search api with parameter b not None should return a bbox tweet."""
        b = "12.2280946,44.1905358,12.3839289,44.308229"
        response = client.get(f"/api/search/?bbox={b}")
        assert response.status_code == 200
        results = response.json()["results"]
        assert len(results) > 0

    def test_with_parameter_b_not_none_should_handle_a_wrong_bbox(self):
        """The search api with parameter b not none should handle a wrong bbox."""
        b = "12.3839289,44.1905358,12.2280946,44.308229"
        response = client.get(f"/api/search/?bbox={b}")
        assert response.status_code == 400
        assert (
            "Northeast corner of bounding_box must be east of southwest corner"
            in response.json()["message"]
        )

    def test_should_return_query_tweets_distribution_in_the_last_month(self):
        """The search api should return query tweets distribution in the last month."""
        response = client.get("/api/search?q=newyork")
        assert response.status_code == 200
        dist = response.json()
        assert "total_tweet_count" in dist["distribution_results"]
        assert "distribution" in dist["distribution_results"]
        assert len(dist["distribution_results"]["distribution"]) == 30
        assert "end" in dist["distribution_results"]["distribution"][0]
        assert "start" in dist["distribution_results"]["distribution"][0]
        assert "count" in dist["distribution_results"]["distribution"][0]

    def test_should_be_able_to_return_a_user_tweets_distribution(self):
        """A search endpoint should be able to return a user tweets distribution."""
        response = client.get("/api/search?u=elonmusk")
        assert response.status_code == 200
        # elonmusk never shuts up
        assert response.json()["distribution_results"]["total_tweet_count"] > 100

    def test_should_return_a_list_of_tweets_with_a_specific_place(self):
        """The search api should return a list of tweets with a specific place."""
        limit = 10
        p = "Canale di Cervia"
        response = client.get(f"/api/search/?p={p}&limit={limit}")
        assert response.status_code == 200
        assert "results" in response.json()
        assert len(response.json()["results"]) <= limit
        if len(response.json()["results"]) > 0:
            tweet = response.json()["results"][0]
            assert tweet["place"] is not None

    def test_should_return_a_list_of_tweets_with_place_and_user(self):
        """The search api should return a list of tweets."""
        limit = 10
        p = "Canale di Cervia"
        u = "Alebarbantiii"
        response = client.get(f"/api/search/?p={p}&u={u}&limit={limit}")
        filtered_response = client.get(
            f"/api/search/?q=geolocalizzati&p={p}&u={u}&limit={limit}"
        )
        assert response.status_code == 200
        assert filtered_response.status_code == 200
        resp = response.json()
        fi_resp = filtered_response.json()
        assert "results" in resp
        assert len(resp["results"]) <= limit
        if len(resp["results"]) > 0:
            tweet = resp["results"][0]
            assert tweet["place"] is not None
        assert resp["results"][0]["user"]["screen_name"] == f"@{u}"
        assert len(resp["results"]) == 2
        assert len(fi_resp["results"]) == 1


class TestASentimentApiEndpoint:
    """Test: A sentiment api endpoint..."""

    def test_should_be_able_to_score_a_text(self):
        """A sentiment api endpoint should be able to score a text."""
        data = {"to_score": [{"text": "I love you", "lang": "en"}]}
        response = client.post(
            f"/api/sentiment",
            data=json.dumps(data),
            headers={"Content-Type": "application/json"},
        )
        assert response.status_code == 200
        resp = response.json()
        assert "scored" in resp
        assert resp["scored"][0]["text"] == data["to_score"][0]["text"]
        assert resp["scored"][0]["lang"] == data["to_score"][0]["lang"]
        assert isinstance(resp["scored"][0]["score"], float)
        assert resp["metrics"]["total_tweets_count"] == 1


@vcr_record
@vcr_delete_on_fail
class TestCompetitionApiEndpoint:
    def test_should_be_able_to_handle_wrong_id(self):
        """Competition api endpoint should be able to return a tree of tweets."""
        id_str = "0"
        response = client.get(f"/api/competition/?id={id_str}")
        response_json = response.json()
        assert response_json["message"] == f"Could not find tweet with id: [{id_str}]."
        assert response.status_code == 404

    def test_should_be_able_to_return_a_tree_of_tweets(self):
        """Competition api endpoint should be able to return a tree of tweets."""
        id_str = "1471781549486059521"
        response = client.get(f"/api/competition/?id={id_str}")
        resp = response.json()
        assert response.status_code == 200
        assert isinstance(resp["root_tweet"], dict)
        assert isinstance(resp["conversation_tree"][1], dict)
        assert resp["conversation_tree"][0]["replies"] is None
        assert isinstance(resp["conversation_tree"][1]["replies"][0], dict)
        assert resp["books"]["sussidiario"] == 1

    def test_should_handle_an_invalid_id(self):
        """Competition api endpoint should andle an invalid id."""
        id_str = "1445426887443066881"
        response = client.get(f"/api/competition/?id={id_str}")
        assert response.status_code == 400
        assert (
            "this tweet is not a valid book competition" in response.json()["message"]
        )


@vcr_record
@vcr_delete_on_fail
class TestTriviaApiEndpoint:
    def test_should_be_able_to_handle_wrong_id(self):
        """Competition api endpoint should be able to return a tree of tweets."""

    id_str = "0"
    response = client.get(f"/api/trivia/?id={id_str}")
    response_json = response.json()
    assert response_json["message"] == f"Could not find tweet with id: [{id_str}]."
    assert response.status_code == 404

    def test_should_be_able_to_return_a_tree_of_tweets(self):
        """Competition api endpoint should be able to return a tree of tweets."""
        id_str = "1479134882957774848"
        response = client.get(f"/api/trivia/?id={id_str}")
        resp = response.json()

        assert response.status_code == 200
        assert isinstance(resp["trivia_tree"][1], dict)
        assert isinstance(resp["trivia_tree"][0]["replies"][0], dict)
        assert isinstance(resp["root_tweet"], dict)
        assert isinstance(resp["questions"][0], dict)
        assert isinstance(resp["questions"][1]["answers"]["A"], dict)
        assert isinstance(resp["placement"], dict)

    def test_should_handle_an_invalid_id(self):
        """Competition api endpoint should andle an invalid id."""
        id_str = "1445426887443066881"
        response = client.get(f"/api/trivia/?id={id_str}")
        assert response.status_code == 400
        assert "this tweet is not a valid Trivia" in response.json()["message"]


# REMEMBER: don't use normal vcr cache with streams. see stream_utils for details
class TestAStreamApi:
    """Test: A Stream API..."""

    def test_should_return_a_stream_response(
        self, cache_callback_stream, parse_truncated_sse
    ):
        """A stream api should return a stream response."""

        response = cache_callback_stream(
            lambda: client.get(f"/api/stream?keywords=cats", stream=True),
            tweets=2,
            skip_cache=False,
        )

        data = parse_truncated_sse(response)
        assert len(data) == 2
        # hashtags is available only in our TweetItem, so check that!
        assert "hashtags" in data[0]

    def test_should_handle_all_filters(
        self, cache_callback_stream, parse_truncated_sse
    ):
        """A stream api should handle all filters."""
        response = cache_callback_stream(
            lambda: client.get(
                f"/api/stream?keywords=cats&usernames=elonmusk&langs=en&locs=-122.7500,36.8000,-121.7500,37.8000",
                stream=True,
            ),
            tweets=0,
            skip_cache=False,
        )
        assert response.status_code == 200

    @staticmethod
    def _patched_factory():
        from backend.stream import get_v2_tweet_from_v1_data_strategy

        return patch(
            "backend.api.get_v2_tweet_from_v1_data_strategy",
            wraps=get_v2_tweet_from_v1_data_strategy,
        )

    @staticmethod
    def _patched_set_rules():
        from backend.api import FilteredRawV1Stream

        return spy_method(FilteredRawV1Stream, "set_rules")

    def test_should_use_locations_as_main_filter_if_present(
        self, cache_callback_stream
    ):
        """A stream api should use locations as main filter if present."""
        #
        with self._patched_factory() as factory:
            with self._patched_set_rules() as set_rules:
                cache_callback_stream(
                    lambda: client.get(
                        f"/api/stream?keywords=cats&usernames=elonmusk&langs=en&locs=-122.7500,36.8000,-121.7500,37.8000",
                        stream=True,
                    ),
                    tweets=0,
                    skip_cache=False,
                )
                right_filters = {
                    "track_keywords": ["cats"],
                    "follow_usernames": ["elonmusk"],
                    "languages": ["en"],
                }
                assert right_filters in factory.call_args_list[0].args
                set_rules.mock.assert_called_with(
                    locations=list(
                        map(
                            lambda x: float(x),
                            "-122.7500,36.8000,-121.7500,37.8000".split(","),
                        )
                    )
                )

    def test_should_use_username_as_second_best_main_filter_if_present(
        self, cache_callback_stream
    ):
        """A stream api should use username as second best main filter if present."""
        with self._patched_factory() as factory:
            with self._patched_set_rules() as set_rules:
                cache_callback_stream(
                    lambda: client.get(
                        f"/api/stream?keywords=cats&usernames=elonmusk&langs=en",
                        stream=True,
                    ),
                    tweets=0,
                    skip_cache=False,
                )
                right_filters = {
                    "track_keywords": ["cats"],
                    "languages": ["en"],
                }
                assert right_filters in factory.call_args_list[0].args
                set_rules.mock.assert_called_with(follow_usernames=["elonmusk"])

    def test_should_correctly_ignore_lang_as_main_filter(self, cache_callback_stream):
        """A stream api should correctly ignore lang as main filter."""
        with self._patched_factory() as factory:
            with self._patched_set_rules() as set_rules:
                cache_callback_stream(
                    lambda: client.get(
                        f"/api/stream?keywords=cats&langs=en",
                        stream=True,
                    ),
                    tweets=0,
                    skip_cache=False,
                )
                right_filters = {
                    "languages": ["en"],
                }
                assert right_filters in factory.call_args_list[0].args
                set_rules.mock.assert_called_with(track_keywords=["cats"])

    def test_should_be_able_to_express_a_location(self, cache_callback_stream):
        """A stream api should be able to express a location."""
        response = cache_callback_stream(
            lambda: client.get(
                f"/api/stream?locs=-122.7500,36.8000,-121.7500,37.8000",
                stream=True,
            ),
            tweets=0,
            skip_cache=False,
        )
        assert response.status_code == 200

    def test_should_error_out_without_filters(self):
        """A stream api should error out without filters."""
        from tests.conftest import truncate_twitter_stream_in_our_sse_relay

        with truncate_twitter_stream_in_our_sse_relay(2):
            response = client.get(f"/api/stream")
        assert response.status_code == 400

    def test_should_error_out_with_only_the_language_as_filter(self):
        """A stream api should error out with only the language as filter."""
        with truncate_twitter_stream_in_our_sse_relay(2):
            response = client.get(f"/api/stream?langs=en")
        assert response.status_code == 400

    def test_should_capture_correctly_errors_from_the_stream(
        self, cache_callback_stream
    ):
        """A stream api should capture correctly errors from the stream."""
        response = cache_callback_stream(
            lambda: client.get(
                f"/api/stream?usernames=verylongusernamethatcantpossiblyexistinthislongform",
                stream=True,
            ),
            tweets=0,
            skip_cache=False,
        )
        assert response.status_code == 400

    def test_should_handle_connection_problem_with_the_stream_provider(self):
        """A stream api should handle connection problem with the stream provider."""
        _check_broken_connection_with_stream_provider("/api/stream?keywords=cats")


# REMEMBER: don't use normal vcr cache with streams. see stream_utils for details
class TestABookCompetitionStreamApi:
    """Test: A book competition stream api..."""

    def test_should_handle_connection_problem_with_the_stream_provider(self):
        """A book competition stream api should handle connection problem with the stream provider."""
        _check_broken_connection_with_stream_provider(
            "/api/competition_stream?id=1471781549486059521"
        )

    def test_should_handle_a_tweet_id_which_is_not_a_competition(self):
        """A book competition stream api should handle a tweet id which is not a competition."""
        with patch(
            "backend.models.book_competition.BookCompetition.check_valid_competition",
            side_effect=InvalidCompetitionIdError(
                "this tweet is not a valid book competition"
            ),
        ):
            with truncate_twitter_stream_in_our_sse_relay(0):
                response = client.get("/api/competition_stream?id=1471781549486059521")
        assert response.status_code == 400
        assert "this tweet is not a valid book competition" in response.text

    # This can use vcr_record since the stream response is mocked with responses.add
    @vcr_record
    @vcr_delete_on_fail
    @responses.activate
    def test_should_return_the_updated_competition_when_needed(
        self, v1_data_samples, cache_callback_stream, parse_truncated_sse
    ):
        """A book competition stream api should return the updated competition when needed."""
        responses.add(
            responses.POST,
            "https://stream.twitter.com/1.1/statuses/filter.json",
            status=200,
            body=f"{v1_data_samples[1]}\nEND-OF-STREAM",
        )
        responses.add_passthru("https://api.twitter.com/")

        response = client.get(
            "/api/competition_stream?id=1471781549486059521",
            stream=True,
        )
        assert response.status_code == 200
        competition_data = parse_truncated_sse(response)
        assert "books" in competition_data[0]


def _check_broken_connection_with_stream_provider(uri: str) -> None:
    from backend.api import FilteredRawV1Stream

    with patch.object(
        FilteredRawV1Stream,
        "get_stream_response",
        side_effect=CannotGetStreamException,
    ):
        with truncate_twitter_stream_in_our_sse_relay(0):
            response = client.get(uri)

    assert response.status_code == 400
    assert "The stream by Twitter could not be obtained" in response.text
