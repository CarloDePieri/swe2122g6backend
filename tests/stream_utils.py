import gzip
import json
import os
import pathlib
from functools import partial
from io import BytesIO
from typing import Callable, Dict, Type
from unittest import mock
from unittest.mock import MagicMock, patch

import pytest
import requests
import responses
import vcr
from vcr.serializers import yamlserializer
import sseclient
from _pytest.fixtures import SubRequest
from requests import Response
from requests.structures import CaseInsensitiveDict
from vcrpy_encrypt import BaseEncryptedPersister


@pytest.fixture
def parse_truncated_sse():
    """Convenience fixture to turn a truncated sse response into a dict"""

    def wrapper(response: Response):
        c = sseclient.SSEClient(response)
        events = list(c.events())
        return list(map(lambda x: json.loads(x.data), events))

    return wrapper


@pytest.fixture
def cache_callback_stream(request, root_directory):
    """Wrapper that can both cache and execute a streaming request. It returns a truncated Response.

    Example
    -------
    response = cache_callback_stream(
        lambda: client.get(f"/api/stream?keywords=cats", stream=True),
        tweets=2,
        skip_cache=False
    )
    assert len(response.iter_lines()) == 4  # 2 tweets data and their matching empty line, per sse protocol
    """

    def wrapped(
        callback: Callable[[], requests.Response], tweets: int, skip_cache: bool = False
    ):
        if skip_cache:
            with truncate_twitter_stream_in_our_sse_relay(tweets):
                return callback()
        else:
            sc = StreamCacher(request, root_directory)
            return sc.cache_callback_stream(callback, tweets)

    return wrapped


class StreamCacher:
    """Provide a wrapper around a requests stream request capable of caching a truncated response. It should be used
    inside a pytest fixture/test that has access to the request fixtures.

    IMPORTANT: it can only be used ONCE in a test and it can contain ONLY ONE stream request in the callback (see
    the example below).

    The output cassettes will be saved to `root_directory`/tests/saved/cached_stream/* with the same scheme of vcrpy.
    They will be clear text with the request headers stripped out (if the response does not contain sensitive data it
    can probably be safely uploaded to vcs).

    This cache is NOT automatically deleted on test failure.

    Example
    -------
    sc = StreamCacher(request, root_directory)
    response = sc.cache_callback_stream(lambda: client.get("/api/stream?keywords=cats"), 2)
    assert len(response.iter_lines()) == 4  # 2 tweets data and their matching empty line, per sse protocol

    Rationale
    ---------
    vcrpy does not handle streaming request yet ( see https://github.com/kevin1024/vcrpy/issues/502 and
    https://github.com/kevin1024/vcrpy/pull/509/commits/917eaccdc73a9cf9fc90eeeb8437b6a70ebb3a59 ). This is a
    workaround.
    """

    cassette_path: str
    persister: Type[BaseEncryptedPersister]

    def __init__(self, request: SubRequest, root_directory: str) -> None:
        test_address = request.node.location[0][6:-3]
        cached_stream_directory = os.path.join(
            root_directory, "tests", "saved", "cached_stream", test_address
        )
        pathlib.Path(cached_stream_directory).mkdir(parents=True, exist_ok=True)
        self.cassette_path = os.path.join(
            cached_stream_directory, f"{request.node.location[2]}.yaml"
        )

        # prepare the encrypted persister. import it here from tests.conftest to avoid circular import
        from tests.conftest import MyEncryptedPersister

        self.persister = MyEncryptedPersister

    def cache_callback_stream(
        self, callback: Callable[[], requests.Response], tweets: int
    ) -> requests.Response:
        """Play the truncated stream cassette, recording it first if needed"""
        if not os.path.isfile(f"{self.cassette_path}.enc"):
            self._record_callback_cassette(callback, tweets)

        # prepare the vcr recorder: it must be in 'new_episodes' mode since all singular tweet requests
        # will need to be recorded as well and added to the existing, manually created cassette
        my_vcr = vcr.VCR(record_mode=["new_episodes"])
        my_vcr.register_persister(self.persister)

        with my_vcr.use_cassette(self.cassette_path):
            return callback()

    def _record_callback_cassette(
        self, callback: Callable[[], requests.Response], tweets: int
    ) -> None:
        """Use a responses.RequestsMock to capture the stream request in the callback, cache it and stop this execution
        of the callback"""
        with responses.RequestsMock() as request_mock:

            def request_callback(req, callback_mock):

                # remove the responses callback to allow a real connection
                callback_mock.add_passthru(req.url)
                callback_mock.remove(req.method, req.url)

                session = requests.Session()
                resp = session.send(req, stream=True)

                self._record_cassette(resp, tweets)

                # Since we are only interested in recording the request, this callback is intended to fail
                raise MissingStreamCacheException

                return 200, {}, ""

            # allow any other connection to twitter TODO find a way to record these as well
            request_mock.add_passthru("https://api.twitter.com/")
            # set the callback for the stream to twitter
            request_mock.add_callback(
                responses.POST,
                "https://stream.twitter.com/1.1/statuses/filter.json",
                callback=partial(request_callback, callback_mock=request_mock),
            )
            try:
                callback()
                # If it got here it means that there was an exception in the callback BEFORE the stream request, so
                # we need to remove the responses callback otherwise it will raise an exception
                request_mock.remove(
                    responses.POST,
                    "https://stream.twitter.com/1.1/statuses/filter.json",
                )
            except MissingStreamCacheException:
                pass

    def _record_cassette(self, response: requests.Response, tweets: int) -> None:
        """Record the cassette of a stream request.

        The request header is intentionally left blank, since the cassette will be left in plain text."""
        body = None
        if tweets > 0:
            response_iterator = response.iter_lines()

            # record only the first `tweets` response lines and put them in a string
            body = ""
            for _ in range(tweets):
                tweet_data = response_iterator.__next__().decode("UTF-8")
                if body == "":
                    body = tweet_data
                else:
                    body += f"\n{tweet_data}"

            # the stream is gzipped
            body = self._zip_payload(body)

        # provide the PreparedRequest with the _to_dict method needed by the yamlserializer
        response.request._to_dict = lambda: {
            "headers": self._prepare_headers(response.request.headers),
            "uri": response.request.url,
            "method": response.request.method,
            "body": None,
        }

        # build the cassette dict
        cassette_data = {
            "requests": [response.request],
            "responses": [
                {
                    "body": {"string": body},
                    "headers": self._prepare_headers(response.headers),
                    "status": {"code": response.status_code, "message": "OK"},
                }
            ],
        }

        # use the encrypted persister to save the cassette
        self.persister.save_cassette(self.cassette_path, cassette_data, yamlserializer)

    @staticmethod
    def _zip_payload(payload: str) -> bytes:
        """gzip the given payload"""
        btsio = BytesIO()
        g = gzip.GzipFile(fileobj=btsio, mode="w")
        g.write(bytes(payload, "utf8"))
        g.close()
        return btsio.getvalue()

    @staticmethod
    def _prepare_headers(headers: CaseInsensitiveDict) -> Dict[str, str]:
        """Convert the headers in a format compatible with vcrpy cassettes"""
        parsed_headers = {}
        for key, value in dict(headers).items():
            v = value
            if isinstance(value, bytes):
                v = v.decode("utf-8")
            parsed_headers[key] = [v]
        return parsed_headers


def truncate_twitter_stream_in_our_sse_relay(tweet_number: int) -> mock._patch:
    """Return a context manager that will force the twitter stream called inside a sse relay to end after
    `tweet_number` tweets.

    Example
    -------
    with truncate_twitter_stream_in_our_sse_relay(2):
        response = client.get("/api/stream?keywords=cats")
    assert len(response.iter_lines()) == 4  # 2 tweets data and their matching empty line, per sse protocol

    Rationale
    ---------
    This despicable kludge is necessary since TestClient can't correctly parse a streaming Response (see
    https://github.com/encode/starlette/issues/1102): a `TestClient.get` with stream=True would block on
    waiting for the data payload to end, instead of preparing iterators.

    As a work-around we mock our own sse relay inner mechanism to only return the first `tweet_number`
    tweets from the Twitter stream and then raise a StopIteration, that will then correctly end the data
    payload, allowing the TestClient.get to unblock."""

    original_iter_lines = Response.iter_lines
    # Using a data structure as counter, so that it can be modified as a side effect from mocked_next
    counter = {"n": tweet_number}

    def mocked_iter_lines(iterator_response):
        iterator = original_iter_lines(iterator_response)

        def mocked_next():
            if counter["n"] > 0:
                counter["n"] -= 1
                return iterator.__next__()
            raise StopIteration

        mocked_iterator = MagicMock(spec=iterator)
        mocked_iterator.__next__.side_effect = mocked_next

        return mocked_iterator

    return patch.object(Response, "iter_lines", mocked_iter_lines)


class MissingStreamCacheException(Exception):
    """Control flow exception, signaling that the needed cache cassette is missing"""
