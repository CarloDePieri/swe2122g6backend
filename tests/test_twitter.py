from typing import List

import pytest
from tweepy import Response, BadRequest

from backend.models import SearchResults, SARequest, SAResponse, TweetItem
from backend.twitter import (
    ApiWrapper,
    search_all,
    UserLookupError,
    get_sentiment_analysis,
    get_tweet_by_id,
    get_book_competition,
    get_trivia,
    InvalidTweetIdException,
)

from tests.conftest import vcr_record, vcr_delete_on_fail


@vcr_record
@vcr_delete_on_fail
class TestATwitterWrapper:
    """Test: A twitter wrapper..."""

    client: ApiWrapper

    @pytest.fixture(scope="class", autouse=True)
    def setup(self, request):
        """TestATwitterApiClient setup"""
        request.cls.client = ApiWrapper()

    def test_should_prepare_authentication_credentials_at_init(self):
        """A twitter wrapper should prepare authentication credentials at init."""
        assert self.client.client.consumer_key is not None
        assert self.client.client.bearer_token is not None
        assert self.client.client.consumer_secret is not None

    def test_should_be_able_to_search_a_tweet(self):
        """A twitter wrapper should be able to search a tweet."""
        query = "unibo"
        limit = 10
        response = self.client.search_all(query, limit=limit)
        assert isinstance(response, Response)
        assert 0 < len(response.data) <= limit
        assert (
            len(list(filter(lambda tweet: query in tweet.text.lower(), response.data)))
            > 0
        )

    def test_should_be_able_to_return_a_user_tweets(self):
        """A twitter wrapper should be able to return a user tweets."""
        username = "elonmusk"
        query = "from:elonmusk"
        limit = 10
        response = self.client.search_all(query, limit=limit)
        assert isinstance(response, Response)
        assert 0 < len(response.data) <= limit
        user_tweets = response.includes["users"]
        for user in user_tweets:
            assert user.username == username

    def test_should_be_able_to_return_the_last_month_results_distribution(self):
        """A twitter wrapper should be able to return the last seven day results distribution."""
        response = self.client.get_tweets_distribution(query="unibo")
        assert isinstance(response, Response)
        assert len(response.data) == 31
        assert "total_tweet_count" in response.meta

    def test_should_be_able_to_get_an_id_from_a_username(self):
        """A twitter wrapper should be able to get an id from a username."""
        _id = self.client.get_username_id(screen_name="elonmusk")
        assert _id == 44196397

    def test_should_handle_invalid_username_when_looking_up_a_user(self):
        """A twitter wrapper should handle invalid username when looking up a user."""
        with pytest.raises(BadRequest):
            self.client.get_username_id(screen_name="asdasdfadfas76as8dfasfs")

    def test_should_handle_non_existing_usernames(self):
        """A twitter wrapper should handle non existing usernames."""
        with pytest.raises(UserLookupError):
            self.client.get_username_id(screen_name="CarloDeBBLoL")

    def test_should_handle_suspended_usernames(self):
        """A twitter wrapper should handle suspended usernames."""
        with pytest.raises(UserLookupError):
            self.client.get_username_id(screen_name="newyork")

    def test_should_handle_an_empty_query(self):
        """A twitter wrapper should handle an empty results query."""
        query = "unibo from:unibo place:Bologna"
        limit = 10
        results = search_all(query, limit=limit)
        assert isinstance(results, SearchResults)
        assert len(results.results) == 0
        assert results.distribution_results.total_tweet_count == 0

    def test_should_handle_an_invalid_place_name(self):
        """A twitter wrapper should handle an invalid place name."""
        query = "unibo from:unibo place:QuelPostoCheNonEsiste123"
        limit = 10
        results = search_all(query, limit=limit)
        assert isinstance(results, SearchResults)
        assert len(results.results) == 0
        assert results.distribution_results.total_tweet_count == 0

    def test_should_be_able_to_lookup_a_single_tweet_by_id(self):
        """A twitter wrapper should be able to lookup a single tweet by id."""
        tweet_response = self.client.get_tweet_by_id("1471781549486059521")
        assert tweet_response.data is not None
        assert (
            tweet_response.data.text
            == "#ingsw2021 #group6newcompetition il libro piu' bello dell'anno"
        )

    def test_should_raise_an_error_when_looking_for_an_invalid_id_tweet(self):
        """A twitter wrapper should raise an error when looking for an invalid id tweet."""
        with pytest.raises(InvalidTweetIdException):
            self.client.get_tweet_by_id("0")


@vcr_record
@vcr_delete_on_fail
class TestTwitterResponseTransformer:
    """Test: Twitter Response Transformer..."""

    def test_should_be_able_to_transform_tweets_from_a_search_query(self):
        """Twitter response transformer should be able to transform tweets from a search query."""
        query = "unibo"
        limit = 10
        results = search_all(query, limit=limit)
        assert isinstance(results, SearchResults)
        assert 0 < len(results.results) <= limit

    def test_should_be_able_to_transform_tweets_from_a_user_timeline(self):
        """Twitter response transformer should be able to transform tweets from a user timeline."""
        query = "from:elonmusk"
        limit = 10
        results = search_all(query=query, limit=limit)
        assert isinstance(results, SearchResults)
        assert 0 < len(results.results) <= limit

    def test_should_return_an_estimated_sentiment_analysis(self):
        """Twitter response transformer should return an estimated sentiment analysis."""
        data = {
            "to_score": [
                {"text": "I love you", "lang": "en"},
            ]
        }
        sa_request = SARequest.parse_obj(data)
        sa_response = get_sentiment_analysis(sa_request)
        assert isinstance(sa_response, SAResponse)

    def test_should_be_able_to_return_a_conversation_tree(self):
        """Twitter response transformer should be able to return a conversation tree."""

        id_str = "1471781549486059521"
        tree = get_book_competition(id_str)
        assert tree.root_tweet.id_str == id_str
        assert tree.conversation_tree[0].replies is None
        assert len(tree.conversation_tree[1].replies) > 0

    def test_should_be_able_to_return_a_single_tweet_by_id(self, geo_tweet):
        """Twitter response transformer should be able to return a single tweet by id."""

        id_str = "1471781549486059521"
        tweet = get_tweet_by_id(id_str)
        assert isinstance(tweet, TweetItem)
        assert tweet.id_str == id_str

    def test_should_be_able_to_return_a_trivia_data(self):
        """Twitter response transformer should be able to return a trivia data."""
        id_str = "1479134882957774848"
        tree = get_trivia(id_str)
        assert tree.root_tweet.id_str == id_str
        assert isinstance(tree.questions, List)
        assert isinstance(tree.questions[0].answers, dict)
