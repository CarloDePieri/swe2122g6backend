import os
from typing import Tuple, List
from unittest.mock import patch

import pytest
import vcr

from time import sleep

from pytest_vcr_delete_on_fail import get_default_cassette_path
from vcrpy_encrypt import BaseEncryptedPersister

from backend.secrets import SecretNotFoundException
from tests.utils import read_saved_object
from tests.stream_utils import (
    cache_callback_stream,
    parse_truncated_sse,
    truncate_twitter_stream_in_our_sse_relay,
)


@pytest.fixture(scope="session", autouse=True)
def root_directory(request):
    """Return the project root directory."""
    return str(request.config.rootdir)


def get_test_cache_key() -> bytes:
    name = "TEST_CACHE_KEY"
    secret = os.getenv(name)
    if secret is None:
        try:
            # If the secret were not in the env, try to get it from the secret file
            import backend.secret_values

            secret = getattr(backend.secret_values, name)
        except ModuleNotFoundError:
            # If the file is not there, raise an error
            raise SecretNotFoundException(
                f"Could not find secret {name} . Either have an environment variable set or put the secret in"
                " backend/secret_values.py (take a look at backend/secret_values.template.py for hints)."
            )
    else:
        secret = secret.encode("UTF-8")
    return secret


class MyEncryptedPersister(BaseEncryptedPersister):
    encryption_key: bytes = get_test_cache_key()
    should_output_clear_text_as_well = True


#
# Default configuration for pytest-recording
#
# record_mode: once - register new cassettes, use existing one but throw an error
#                       if an existing one try to use different web resources
#
@pytest.fixture(scope="module")
def vcr_config():
    return {"record_mode": ["once"]}


#
# Configuration hook for pytest-recording: in here the vcr object can be accessed
#
def pytest_recording_configure(vcr):
    vcr.register_persister(MyEncryptedPersister)


def get_project_root() -> str:
    root = os.path.dirname(__file__)[:-5]
    return root


# Define two helper functions that will take the default path and append vcrpy_encrypt suffixes
def get_encrypted_cassette(item) -> str:
    root = get_project_root()
    default = get_default_cassette_path(item)
    return f"{root}{default}{MyEncryptedPersister.encoded_suffix}"


def get_clear_text_cassette(item) -> str:
    root = get_project_root()
    default = get_default_cassette_path(item)
    return f"{root}{default}"


# Define a shorthand for the vcr_delete_on_fail marker
vcr_delete_on_fail = pytest.mark.vcr_delete_on_fail(
    [get_encrypted_cassette, get_clear_text_cassette]
)
# Decorator used as a shortcut for vcr_delete_test_cassette_on_failure fixture
vcr_delete_on_failure = pytest.mark.usefixtures("vcr_delete_test_cassette_on_failure")
# Decorator used for consistency
vcr_record = pytest.mark.vcr
# custom vcr
my_vcr = vcr.VCR(record_mode=["once"])
my_vcr.register_persister(MyEncryptedPersister)


#
# MOCKED OBJECTS
#
# The problem here is that pydantic models does not let MagickMock objects pass its constructor checks, so we will have
# to find a way around that...
# For now simply use cached http request and recreate tweepy object from those.
# BEWARE: since these are cached objects, these tests will fail if the tweepy or twitter apis change
#
@pytest.fixture(scope="session")
def get_cassette_path(root_directory):
    """Return the full cassette path"""

    def wrapper(name: str) -> Tuple[str, str]:
        cassette = f"{root_directory}/tests/cassettes/{name}.yaml"
        encrypted_cassette = f"{root_directory}/tests/cassettes/{name}.yml.enc"
        return cassette, encrypted_cassette

    return wrapper


@pytest.fixture(scope="session", autouse=True)
def selected_tweets(get_cassette_path):
    """For v2.0 tweet status: Pickle can't load them. We retrieve our selected tweets at the start of the session."""
    from backend.twitter import ApiWrapper

    api = ApiWrapper()

    tweets = {
        "with_geo": "Questo è un tweet geolocalizzato in teoria #ingsw2021 from:alebarbantiii",
        "with_place": "Non so fare i tweet geolocalizzati #ingsw2021 from:alebarbantiii",
        "with_media": "Playing with game of life #ingsw2021 from:CarloDePieri",
        "with_retweet": "Questo è un tweet geolocalizzato in teoria #ingsw2021 from:CarloDePieri",
        "with_quote": "Test di retweet con citazione #ingsw2021 from:CarloDePieri",
        "with_reply": "risposta ad un tweet #ingsw2021 from:CarloDePieri",
        "with_poll": "E tu da che parte stai? #IngSw2021 from:Sir_Sghino",
    }

    for tweet_key, tweet_text in tweets.items():
        cassette_path, encrypted_cassette_path = get_cassette_path(
            f"test_mock_tweet_{tweet_key}"
        )

        # If the cache DOES NOT already exist, these api call should have a time buffer between them
        should_wait_between_calls = not (
            os.path.isfile(encrypted_cassette_path) or os.path.isfile(cassette_path)
        )

        with my_vcr.use_cassette(cassette_path):
            # don't use api.search_all directly or it will be uncomfortable to debug
            tweets[tweet_key] = api.client.search_all_tweets(
                query=tweet_text,
                max_results=10,
                start_time="2021-01-01T00:00:00Z",
                **api.tweet_data_model,
            )

        if should_wait_between_calls:
            sleep(1)

    with my_vcr.use_cassette(get_cassette_path("test_mock_distribution")[0]):
        tweets["distribution"] = api.client.get_all_tweets_count(
            query="unibo", granularity="day"
        )

    return tweets


@pytest.fixture
def mock_distribution(selected_tweets):
    return selected_tweets["distribution"]


@pytest.fixture
def reply_tweet(selected_tweets):
    return selected_tweets["with_reply"]


@pytest.fixture
def media_tweet(selected_tweets):
    return selected_tweets["with_media"]


@pytest.fixture
def retweet_tweet(selected_tweets):
    return selected_tweets["with_retweet"]


@pytest.fixture
def quote_tweet(selected_tweets):
    return selected_tweets["with_quote"]


@pytest.fixture
def place_tweet(selected_tweets):
    return selected_tweets["with_place"]


@pytest.fixture
def geo_tweet(root_directory, selected_tweets):
    return selected_tweets["with_geo"]


@pytest.fixture
def poll_tweet(root_directory, selected_tweets):
    return selected_tweets["with_poll"]


@pytest.fixture
def fake_distribution(root_directory):
    return read_saved_object("distribution", f"{root_directory}/tests/saved")


@pytest.fixture(scope="class")
def c_fake_distribution(root_directory):
    return read_saved_object("distribution", f"{root_directory}/tests/saved")


def spy_method(_object, method_name):
    """Add a mock field to a method that can be used to spy the method invocations"""

    def spy_decorator(method_to_decorate):
        from unittest.mock import MagicMock

        mock = MagicMock()

        def wrapper(self, *args, **kwargs):
            mock(*args, **kwargs)
            return method_to_decorate(self, *args, **kwargs)

        wrapper.mock = mock
        return wrapper

    spied = spy_decorator(getattr(_object, method_name))

    return patch.object(_object, method_name, spied)


@pytest.fixture(scope="session", autouse=True)
def v1_data_samples(root_directory):
    samples_path = os.path.join(root_directory, "tests", "saved", "v1_data_samples")
    samples: List[str] = []
    with open(samples_path, "r") as f:
        for line in f:
            samples.append(line)
    return samples
