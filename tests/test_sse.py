import asyncio
import logging
from typing import Optional

import responses
import requests
import pytest
from unittest.mock import MagicMock

from fastapi import FastAPI, Request
from fastapi.testclient import TestClient
from sse_starlette.sse import EventSourceResponse

from backend.sse import ServerSideEventRelay


@pytest.fixture
def mocked_responses():
    with responses.RequestsMock() as rsps:
        yield rsps


@pytest.fixture
def mocked_stream_response(mocked_responses):
    mocked_responses.add(
        responses.GET,
        "http://twitter.com/api/1/foobar",
        status=200,
        body="first\nsecond\nEND-OF-STREAM",
    )
    return requests.get("http://twitter.com/api/1/foobar", stream=True)


@pytest.fixture
def mocked_broken_stream_response(mocked_stream_response):
    def mocked_next(_):
        raise asyncio.CancelledError

    mock_iterator = MagicMock()
    mock_iterator.__next__ = mocked_next
    broken_response = MagicMock(spec=mocked_stream_response)
    broken_response.iter_lines.return_value = mock_iterator
    return broken_response


@pytest.fixture
def mocked_api_request():
    from fastapi import Request
    from unittest.mock import MagicMock

    req = MagicMock(spec=Request)

    async def mocked_is_disconnect():
        return False

    req.is_disconnected = mocked_is_disconnect
    return req


@pytest.fixture
def mocked_api_request_disconnection(mocked_api_request):
    async def mocked_disconnect():
        return True

    disconnected = MagicMock(spec=mocked_api_request)
    disconnected.is_disconnected = mocked_disconnect
    return disconnected


class TestASSERelay:
    """Test: A SSERelay..."""

    def test_should_build_an_event_source_response(
        self, mocked_stream_response, mocked_api_request
    ):
        """A sse relay should build an EventSourceResponse."""
        sse = ServerSideEventRelay(mocked_stream_response, mocked_api_request)
        source = sse.build_event_source_response()
        assert isinstance(source, EventSourceResponse)

    def test_should_handle_a_client_disconnect(
        self, mocked_stream_response, mocked_api_request_disconnection, caplog
    ):
        """A sse relay should handle a client disconnect."""
        caplog.set_level(logging.INFO)
        app = FastAPI()

        @app.get("/stream")
        async def stream(request: Request) -> EventSourceResponse:
            sse = ServerSideEventRelay(
                mocked_stream_response, mocked_api_request_disconnection
            )
            return sse.build_event_source_response()

        client = TestClient(app)
        resp = client.get("/stream")
        assert "[Stream Started] Source connected" in caplog.text
        # NOTE: this line triggers iter_lines.__next__() which then will trigger the check on the client disconnection
        assert len(list(resp.iter_lines())) == 0
        assert "[Stream Stopped] Api client disconnected" in caplog.text

    def test_should_handle_a_broken_connection(
        self, mocked_broken_stream_response, caplog
    ):
        """A sse relay should handle a broken connection."""

        caplog.set_level(logging.INFO)
        app = FastAPI()

        @app.get("/stream")
        async def stream(request: Request) -> EventSourceResponse:
            sse = ServerSideEventRelay(mocked_broken_stream_response, request)
            return sse.build_event_source_response()

        client = TestClient(app)
        resp = client.get("/stream")
        assert "[Stream Started] Source connected" in caplog.text
        # NOTE: this line triggers iter_lines.__next__() which then simulates a source asyncio.CancelledError
        assert len(list(resp.iter_lines())) == 0
        assert "[Stream Stopped] Error: source disconnected" in caplog.text

    def test_should_handle_a_simple_api(self, mocked_stream_response):
        """A sse relay should handle a simple api."""

        app = FastAPI()

        @app.get("/stream")
        async def stream(request: Request) -> EventSourceResponse:
            sse = ServerSideEventRelay(mocked_stream_response, request)
            return sse.build_event_source_response()

        client = TestClient(app)

        resp = client.get("/stream")
        assert resp.status_code == 200
        # NOTE: sse protocol needs a blank line between every data element, so the following is correct
        streamed = [
            b"data: first",
            b"",
            b"data: second",
            b"",
            b"data: END-OF-STREAM",
            b"",
        ]
        for line in resp.iter_lines():
            assert line == streamed.pop(0)

    def test_should_handle_a_transformation_strategy(self, mocked_stream_response):
        """A sse relay should handle a transformation strategy."""

        app = FastAPI()

        @app.get("/stream")
        async def stream(request: Request) -> EventSourceResponse:
            sse = ServerSideEventRelay(mocked_stream_response, request)
            return sse.build_event_source_response(strategy=lambda x: x.upper())

        client = TestClient(app)
        resp = client.get("/stream")
        assert resp.status_code == 200
        streamed = [
            b"data: FIRST",
            b"",
            b"data: SECOND",
            b"",
            b"data: END-OF-STREAM",
            b"",
        ]
        for line in resp.iter_lines():
            assert line == streamed.pop(0)

    def test_should_handle_a_filter_strategy(self, mocked_stream_response):
        """A sse relay should handle a filter strategy."""

        app = FastAPI()

        @app.get("/stream")
        async def stream(request: Request) -> EventSourceResponse:
            sse = ServerSideEventRelay(mocked_stream_response, request)

            def _filter(x: str) -> Optional[str]:
                if x != "second":
                    return x

            return sse.build_event_source_response(strategy=_filter)

        client = TestClient(app)
        resp = client.get("/stream")
        assert resp.status_code == 200
        streamed = [
            b"data: first",
            b"",
            b"data: END-OF-STREAM",
            b"",
        ]
        for line in resp.iter_lines():
            assert line == streamed.pop(0)
