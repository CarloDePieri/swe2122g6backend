from backend.models import SARequest, SAResponse
from backend.twitter import search_all
from tests.conftest import vcr_record, vcr_delete_on_fail


def test_should_be_able_to_create_a_scored_response_from_a_request():
    """should be able to create a scored response from a request"""
    data = {
        "to_score": [
            {"text": "I love you", "lang": "en"},
            {"text": "I hate you", "lang": "en"},
            {"text": "Neutral thought", "lang": "en"},
        ]
    }
    sa_request = SARequest.parse_obj(data)
    sa_response = SAResponse.from_request(sa_request)
    assert sa_response.scored[0].text == data["to_score"][0]["text"]
    assert sa_response.scored[0].lang == data["to_score"][0]["lang"]
    assert isinstance(sa_response.scored[0].score, float)
    assert sa_response.metrics.total_tweets_count == 3
    assert sa_response.metrics.positive_tweets_count == 1
    assert sa_response.metrics.negative_tweets_count == 1
    assert sa_response.metrics.neutral_tweets_count == 1


@vcr_record
@vcr_delete_on_fail
def test_should_be_able_to_score_a_bunch_of_tweets():
    """should be able to score a bunch of tweets"""
    # NOTE: this is an expensive test to run all the time... 20 is dangerously low (statistically speaking), but it
    # allows the cached test to complete in ~1.20s
    tweets = search_all("novax", 30)
    data = {
        "to_score": list(
            map(lambda x: {"text": x.text, "lang": x.lang}, tweets.results)
        )
    }
    sa_request = SARequest.parse_obj(data)
    sa_response = SAResponse.from_request(sa_request)
    # novax should return mainly negative tweets
    assert (
        sa_response.metrics.negative_tweets_count
        > sa_response.metrics.positive_tweets_count
    )
