import pytest

from backend.models import DistributionResults, TweetsDateCount


class TestADistributionObject:
    """Test: A Distribution object..."""

    @pytest.fixture(scope="class", autouse=True)
    def setup(self, request, c_fake_distribution):
        """TestADistributionObject setup"""
        request.cls.fake = c_fake_distribution
        request.cls.dr = DistributionResults.from_response(c_fake_distribution)

    def test_should_be_able_to_represent_a_single_day(self):
        """A distribution object should be able to represent a single day."""
        assert isinstance(self.dr.distribution[0], TweetsDateCount)
        assert self.dr.distribution[0].start == self.fake.data[1]["start"]
        assert self.dr.distribution[0].end == self.fake.data[1]["end"]
        assert self.dr.distribution[0].count == self.fake.data[1]["tweet_count"]

    def test_should_be_built_correctly(self):
        """A distribution object should be built correctly."""
        assert len(self.dr.distribution) == 7
        assert self.dr.total_tweet_count == self.fake.meta["total_tweet_count"]
