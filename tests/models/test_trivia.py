import pytest

from backend.models.trivia import InvalidIdErrorTrivia, Trivia, Place
from backend.twitter import get_trivia, search_all, InvalidTweetIdException
from tests.conftest import vcr_record, vcr_delete_on_fail


@vcr_record
@vcr_delete_on_fail
class TestTrivia:
    def test_should_return_a_list_of_replies_of_a_tweet(self):
        """Twitter response transformer should return a list of replies of a tweet."""
        trivia = search_all("#group6newtrivia", limit=10)
        query = f"conversation_id:{trivia.results[0].conversation_id}"
        interaction = search_all(query, limit=30)
        tree = Trivia.from_interaction(
            interaction,
            trivia.results[0],
        )
        assert tree.root_tweet == trivia.results[0]
        assert len(tree.trivia_tree[0].replies) > 2
        assert len(tree.trivia_tree[1].replies) > 2

    def test_should_have_only_answer_older_than_the_expiration_date(self):
        """Trivia should have only answer older than the expiration date."""
        id_str = "1479134882957774848"
        tree = get_trivia(id_str)
        for answer in tree.trivia_tree[1].replies:
            assert answer.created_at <= "2022-01-11T13:34:44.000Z"

    def test_answers_and_questions_should_have_their_hashtags(self):
        """Trivia answers and questions should have their hashtags."""
        id_str = "1479134882957774848"
        tree = get_trivia(id_str)
        for question in tree.trivia_tree:
            assert "#group6newquestion" in question.text
            for answer in question.replies:
                assert isinstance(tree.placement[answer.user.screen_name], Place)
                assert "#group6answer" in answer.text

    def test_should_handle_an_invalid_tweet_id(self):
        """Twitter response transformer should handle an invalit tweet id."""
        id_str = "1445426887443066881"
        with pytest.raises(InvalidIdErrorTrivia):
            get_trivia(id_str)

    def test_should_raise_an_error_when_looking_for_an_invalid_id_tweet(self):
        """A twitter wrapper should raise an error when looking for an invalid id tweet."""
        id_str = "0"
        with pytest.raises(InvalidTweetIdException):
            get_trivia(id_str)
