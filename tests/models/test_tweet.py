from backend.models import (
    TweetGeo,
    TweetItem,
    TweetPlace,
    TweetUser,
    TweetMetrics,
    TweetMedia,
    TweetReferenced,
    TweetMentions,
)


class TestATweetItem:
    """Test: A TweetItem..."""

    def test_should_be_built_correctly(self, geo_tweet):
        """A tweet item should be built correctly."""
        tweet = TweetItem.from_data(geo_tweet[0][0], geo_tweet.includes)
        assert tweet.text == geo_tweet.data[0].text
        assert tweet.lang == geo_tweet.data[0].lang
        assert tweet.id_str == str(geo_tweet.data[0].id)
        assert tweet.created_at == geo_tweet.data[0].data["created_at"]
        assert tweet.possibly_sensitive == geo_tweet.data[0].possibly_sensitive
        assert tweet.hashtags[0] == geo_tweet.data[0].entities["hashtags"][0]["tag"]
        assert isinstance(tweet.user, TweetUser)
        assert isinstance(tweet.metrics, TweetMetrics)

    def test_should_include_conversation_id_parameter(self, geo_tweet):
        """A tweet item should has conversation id parameter."""
        tweet = TweetItem.from_data(geo_tweet[0][0], geo_tweet.includes)
        assert tweet.conversation_id == geo_tweet[0][0].conversation_id

    def test_should_include_urls_when_present(self, quote_tweet):
        """A tweet item should include urls when present."""
        tweet = TweetItem.from_data(quote_tweet[0][0], quote_tweet.includes)
        assert tweet.urls[0] == quote_tweet[0][0].entities["urls"][0]["url"]

    def test_should_include_geo_information_when_present(self, geo_tweet):
        """A tweet item should include geo information when present."""
        tweet = TweetItem.from_data(geo_tweet[0][0], geo_tweet.includes)
        # Should include both geo and place here!
        assert isinstance(tweet.geo, TweetGeo)
        assert isinstance(tweet.place, TweetPlace)

    def test_should_include_place_information_when_present(self, place_tweet):
        """A tweet item should include place information when present."""
        tweet = TweetItem.from_data(place_tweet[0][0], place_tweet.includes)
        # Should only include the Place object
        assert isinstance(tweet.place, TweetPlace)
        assert tweet.geo is None

    def test_should_include_media_information_when_present(self, media_tweet):
        """A tweet item should include media information when present."""
        tweet = TweetItem.from_data(media_tweet[0][0], media_tweet.includes)
        assert len(tweet.media) > 0
        assert isinstance(tweet.media[0], TweetMedia)

    def test_should_include_referenced_tweets_if_present(self, retweet_tweet):
        """A tweet item should include referenced tweets if present."""
        tweet = TweetItem.from_data(retweet_tweet[0][0], retweet_tweet.includes)
        assert isinstance(tweet.referenced_tweet, TweetReferenced)

    def test_should_include_mentions_when_present(self, retweet_tweet):
        """A tweet item should include mentions when present."""
        tweet = TweetItem.from_data(retweet_tweet[0][0], retweet_tweet.includes)
        assert len(tweet.mentions) > 0
        assert isinstance(tweet.mentions[0], TweetMentions)

    def test_should_handle_different_attachments(self, poll_tweet):
        """A tweet item should handle different attachments."""
        tweet = TweetItem.from_data(poll_tweet.data[0], poll_tweet.includes)
        assert isinstance(tweet, TweetItem)
        assert tweet.media is None
