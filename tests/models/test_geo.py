from backend.models import Coordinates, TweetGeo, TweetBoundingBox, TweetPlace


class TestGeoModels:
    """Test: GeoModels..."""

    def test_should_be_able_to_create_a_coordinates_obj(self):
        """Geo models should be able to create a coordinates obj."""
        data = [44.0, 22.0]
        coord = Coordinates.from_list(data)
        assert coord.lat == data[1]
        assert coord.lon == data[0]

    def test_should_be_able_to_create_a_geo_object_from_a_twitter_dict(self, geo_tweet):
        """Geo models should be able to create a Geo object from a twitter dict."""
        mock = geo_tweet[0][0].geo["coordinates"]
        geo = TweetGeo.from_dict(mock)
        assert geo.type == mock["type"]
        assert geo.coordinates.lat == mock["coordinates"][1]
        assert geo.coordinates.lon == mock["coordinates"][0]

    def test_should_be_able_to_create_a_bounding_box_object(self, geo_tweet):
        """Geo models should be able to create a bounding box object."""
        mock = geo_tweet.includes["places"][0].data["geo"]["bbox"]
        bb = TweetBoundingBox.from_bb(mock)
        assert bb.center.lat == (mock[1] + mock[3]) / 2
        assert bb.bbox == mock

    def test_should_be_able_to_create_a_tweet_place_object(self, geo_tweet):
        """Geo models should be able to create a tweet Place object."""
        place = TweetPlace.from_place(geo_tweet.includes["places"][0])
        assert place.id == geo_tweet.includes["places"][0].id
        assert place.country == geo_tweet.includes["places"][0].country
        assert place.country_code == geo_tweet.includes["places"][0].country_code
        assert place.place_type == geo_tweet.includes["places"][0].place_type
        assert place.name == geo_tweet.includes["places"][0].name
        assert place.full_name == geo_tweet.includes["places"][0].full_name
        assert (
            place.bounding_box.bbox[0]
            == geo_tweet.includes["places"][0].data["geo"]["bbox"][0]
        )
