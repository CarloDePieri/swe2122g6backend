from backend.models import TweetUser


def test_should_be_able_to_create_a_tweet_user_obj(geo_tweet):
    """should be able to create a tweet user obj"""
    user = TweetUser.from_user(geo_tweet.includes["users"][0])
    assert user.name == geo_tweet.includes["users"][0].name
    assert user.img == geo_tweet.includes["users"][0].profile_image_url
    assert user.id == geo_tweet.includes["users"][0].id
    username = geo_tweet.includes["users"][0].username
    assert user.screen_name == f"@{username}"
