from backend.models import TweetReferenced


class TestAReferencedTweet:
    """Test: A referenced tweet..."""

    def test_should_be_able_to_reference_a_retweet(self, retweet_tweet):
        """A referenced tweet should be able to reference a retweet."""
        retweet = TweetReferenced.from_retweet(retweet_tweet[0][0].referenced_tweets[0])
        assert retweet.id == retweet_tweet[0][0].referenced_tweets[0].id
        assert retweet.type == retweet_tweet[0][0].referenced_tweets[0].type
        assert retweet.type == "retweeted"

    def test_should_be_able_to_reference_a_quoted_tweet(self, quote_tweet):
        """A referenced tweet should be able to reference a quoted tweet."""
        quote = TweetReferenced.from_retweet(quote_tweet[0][0].referenced_tweets[0])
        assert quote.id == quote_tweet[0][0].referenced_tweets[0].id
        assert quote.type == quote_tweet[0][0].referenced_tweets[0].type
        assert quote.type == "quoted"

    def test_should_be_able_to_reference_a_replied_to_tweet(self, reply_tweet):
        """A referenced tweet should be able to reference a replied to tweet."""
        reply = TweetReferenced.from_retweet(reply_tweet[0][0].referenced_tweets[0])
        assert reply.id == reply_tweet[0][0].referenced_tweets[0].id
        assert reply.type == reply_tweet[0][0].referenced_tweets[0].type
        assert reply.type == "replied_to"
