import pytest

from backend.models.book_competition import BookCompetition, InvalidCompetitionIdError

from backend.twitter import search_all, get_book_competition, InvalidTweetIdException
from tests.conftest import vcr_record, vcr_delete_on_fail


@vcr_record
@vcr_delete_on_fail
class TestBookCompetition:
    def test_should_return_a_list_of_replies_of_a_tweet(self):
        """Twitter response transformer should return a list of replies of a tweet."""
        competitions = search_all("#group6newcompetition", limit=10)
        query = f"conversation_id:{competitions.results[0].conversation_id}"
        conversation = search_all(query, limit=10)
        tree = BookCompetition.from_conversation(
            conversation,
            competitions.results[0],
        )
        assert tree.root_tweet == competitions.results[0]
        assert tree.conversation_tree[0].replies is None
        assert len(tree.conversation_tree[1].replies) > 0

    def test_should_be_able_to_detect_expired_dates(self, geo_tweet):
        """Twitter response transformer should be able to detect expired dates."""
        data = geo_tweet[0][0]["data"]["created_at"]
        expiration_date = BookCompetition._create_expiration_date(data)
        assert "2022-01-06T15:48:28.001Z" > expiration_date
        assert "2022-01-06T15:48:27.999Z" < expiration_date

    def test_should_handle_an_invalid_tweet_id(self):
        """Twitter response transformer should handle an invalit tweet id."""
        id_str = "1445426887443066881"
        with pytest.raises(InvalidCompetitionIdError):
            get_book_competition(id_str)

    def test_should_raise_an_error_when_looking_for_an_invalid_id_tweet(self):
        """A twitter wrapper should raise an error when looking for an invalid id tweet."""
        id_str = "0"
        with pytest.raises(InvalidTweetIdException):
            get_book_competition(id_str)
