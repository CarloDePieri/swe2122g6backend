from backend.models import TweetMetrics


def test_should_be_able_to_create_a_metrics_obj(geo_tweet):
    """should be able to create a metrics obj"""
    metrics = TweetMetrics.from_metrics(geo_tweet[0][0].public_metrics)
    assert metrics.like == geo_tweet[0][0].public_metrics["like_count"]
    assert metrics.reply == geo_tweet[0][0].public_metrics["reply_count"]
    assert metrics.retweet == geo_tweet[0][0].public_metrics["retweet_count"]
    assert metrics.quote == geo_tweet[0][0].public_metrics["quote_count"]
