from backend.models import SearchResults, DistributionResults, TweetItem


def test_should_be_able_to_create_a_search_results_obj(geo_tweet, mock_distribution):
    """should be able to create a search results obj"""
    sr = SearchResults.from_responses(geo_tweet, mock_distribution)
    assert len(sr.results) == 1
    assert isinstance(sr.distribution_results, DistributionResults)
    assert isinstance(sr.results[0], TweetItem)
    assert sr.results[0].id_str == geo_tweet[0][0].data["id"]
