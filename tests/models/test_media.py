from backend.models import TweetMedia


def test_should_be_able_to_create_a_media_object(media_tweet):
    """should be able to create a media object"""
    media = TweetMedia.from_media(media_tweet.includes["media"][0])
    assert media_tweet.includes["media"][0].data["url"] == media.url
    assert media_tweet.includes["media"][0].data["type"] == media.type
