from backend.models import TweetMentions


def test_should_be_able_to_create_a_tweet_mentions_obj(retweet_tweet):
    """should be able to create a TweetMentions object"""
    mentions = TweetMentions.from_mentions(retweet_tweet[0][0].entities["mentions"][0])
    assert retweet_tweet[0][0].entities["mentions"][0]["id"] == mentions.id
