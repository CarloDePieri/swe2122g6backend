import base64
import os
from typing import Callable
from unittest.mock import MagicMock, patch
import pytest

from backend.secrets import Secrets, SecretNotFoundException


def _encode(clear_text_string: str) -> str:
    """Utility function to quickly encode from base64."""
    return base64.b64encode(clear_text_string.encode("utf-8")).decode("utf-8")


# Dict used to mock the environment
FAKE_SECRETS = {
    "KEY": _encode("env: my_consumer_key"),
    "KEY_SECRET": _encode("env: consumer_secret"),
    "ACCESS_TOKEN": _encode("env: access_token"),
    "ACCESS_TOKEN_SECRET": _encode("env: access_token_secret"),
    "BEARER_TOKEN": _encode("env: bearer_token"),
    "SECONDARY_KEY": _encode("env: my_consumer_key"),
    "SECONDARY_KEY_SECRET": _encode("env: consumer_secret"),
    "SECONDARY_ACCESS_TOKEN": _encode("env: access_token"),
    "SECONDARY_ACCESS_TOKEN_SECRET": _encode("env: access_token_secret"),
    "SECONDARY_BEARER_TOKEN": _encode("env: bearer_token"),
}

# Mocked secret module
FAKE_MODULE = MagicMock()
for secret_key, secret_value in FAKE_SECRETS.items():
    setattr(
        FAKE_MODULE.secret_values,
        secret_key,
        _encode(Secrets().decode(secret_value).replace("env:", "file:")),
    )


def _get_getenv_mock(positive_outcome: bool = True) -> Callable:
    """Return a mocked os.getenv function that, based on `positive_outcome` will either return a fake value or None."""

    orig_getenv = os.getenv  # Store the original getenv

    def getenv_mock(name, *args, **kwargs):
        if name in FAKE_SECRETS:
            if positive_outcome:
                return FAKE_SECRETS[name]
            else:
                return None
        return orig_getenv(*args, **kwargs)

    return getenv_mock


@pytest.fixture
def without_env_secrets():
    """Ensure that no environment secret is present in the test."""

    mocked_getenv_with_negative_outcome = _get_getenv_mock(positive_outcome=False)

    with patch("os.getenv", side_effect=mocked_getenv_with_negative_outcome):
        yield


@pytest.fixture
def with_env_secrets():
    """Ensure that mocked environment secrets are present in the test."""

    mocked_getenv_with_positive_outcome = _get_getenv_mock()

    with patch("os.getenv", side_effect=mocked_getenv_with_positive_outcome):
        yield


def _get_import_mock(positive_outcome: bool = True) -> Callable:
    """Return a mocked import function that, based on `positive_outcome` will either return a fake module or raise
    a ModuleNotFoundError."""

    orig_import = __import__  # Store the original import

    def import_mock(name, *args):
        if name == "backend.secret_values":
            if positive_outcome:
                return FAKE_MODULE
            else:
                raise ModuleNotFoundError
        return orig_import(name, *args)

    return import_mock


@pytest.fixture
def with_file_secrets():
    """Ensure that a mocked secret module are present in the test."""

    mocked_import_with_positive_outcome = _get_import_mock()

    with patch("builtins.__import__", side_effect=mocked_import_with_positive_outcome):
        yield


@pytest.fixture
def without_file_secrets():
    """Ensure that no secret module is present in the test."""

    mocked_import_with_negative_outcome = _get_import_mock(positive_outcome=False)

    with patch("builtins.__import__", side_effect=mocked_import_with_negative_outcome):
        yield


class TestASecretsClass:
    """Test: A Secrets class..."""

    @staticmethod
    def _assert_all_values_are_taken_from_env(secrets: Secrets) -> None:
        for key in FAKE_SECRETS.keys():
            assert getattr(secrets, key.lower()) == secrets.decode(FAKE_SECRETS[key])

    def test_should_recover_all_secrets_from_the_environment(
        self, with_env_secrets, without_file_secrets
    ):
        """A secrets class should recover all secrets from the environment."""
        self._assert_all_values_are_taken_from_env(Secrets())

    def test_should_give_precedence_to_environment_over_files(
        self, with_env_secrets, with_file_secrets
    ):
        """A secrets class should give precedence to environment over files."""
        self._assert_all_values_are_taken_from_env(Secrets())

    def test_should_recover_all_secrets_from_a_secret_file(
        self, without_env_secrets, with_file_secrets
    ):
        """A secrets class should recover all secrets from a secret file."""
        secrets = Secrets()
        for key in FAKE_SECRETS.keys():
            assert getattr(secrets, key.lower()) == secrets.decode(
                FAKE_SECRETS[key]
            ).replace("env:", "file:")

    def test_should_raise_an_error_if_needed_secrets_are_not_found(
        self, without_env_secrets, without_file_secrets
    ):
        """A secrets class should raise an error if needed secrets are not found."""
        with pytest.raises(SecretNotFoundException):
            Secrets()
