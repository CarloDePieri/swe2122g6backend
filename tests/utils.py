import pickle
import tweepy
from backend.secrets import Secrets
import base64


def save_mock_distribution(query: str) -> None:
    secrets = Secrets()
    client = tweepy.Client(
        bearer_token=secrets.bearer_token,
        access_token=secrets.access_token,
        access_token_secret=secrets.access_token_secret,
        consumer_key=secrets.key,
        consumer_secret=secrets.key_secret,
    )
    resp = client.get_recent_tweets_count(query=query, granularity="day")
    save_object(resp, "distribution")


def save_object(obj: object, name: str, path: str = ".") -> None:
    obj_file = f"{path}/{name}-object.pkl"
    with open(obj_file, "wb+") as f:
        pickle.dump(obj, f, -1)
        print("Object dumped! Make sure it's in tests/saved")


def read_saved_object(name: str, path: str) -> object:
    obj_path = f"{path}/{name}-object.pkl"
    with open(obj_path, "rb") as f:
        return pickle.load(f)


def encode_secret(secret: str) -> str:
    return base64.b64encode(secret.encode("utf-8")).decode("utf-8")
