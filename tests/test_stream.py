import pytest
import json

import responses

from backend.stream import (
    FilteredRawV1Stream,
    CannotGetStreamException,
    get_v2_tweet_from_v1_data_strategy,
    get_competition_stream_strategy,
)
from backend.twitter import get_tweet_by_id
from tests.conftest import vcr_record, vcr_delete_on_fail


class TestAFilteredV1Stream:
    """Test: A filtered v1 stream..."""

    def test_should_be_able_to_set_rules(self):
        """A filtered v1 stream should be able to set rules."""
        stream = FilteredRawV1Stream()
        stream.set_rules(
            track_keywords=["cats", "dogs"],
            follow_usernames=["elonmusk", "thepope", "zgyxuvgasd"],
            locations=[-122.75, 36.8, -121.75, 37.8],
        )
        assert stream.get_rules() == {
            "track": "cats,dogs",
            "follow": "44196397,1148121",
            "locations": "-122.7500,36.8000,-121.7500,37.8000",
        }

    def test_should_successfully_authenticate(self, cache_callback_stream):
        """A filtered v1 stream should successfully authenticate."""
        stream = FilteredRawV1Stream()
        stream.set_rules(track_keywords=["cats"])
        resp = cache_callback_stream(lambda: stream.get_stream_response(), 0)
        assert resp.status_code == 200

    def test_should_successfully_return_tweets(self, cache_callback_stream):
        """A filtered v1 stream should successfully return tweets."""
        stream = FilteredRawV1Stream()
        stream.set_rules(track_keywords=["cats"])
        resp = cache_callback_stream(lambda: stream.get_stream_response(), 1)
        tweet = json.loads(resp.iter_lines().__next__())
        assert tweet["id_str"] is not None

    def test_should_be_able_to_filter_username(self, cache_callback_stream):
        """A filtered v1 stream should be able to filter username."""
        stream = FilteredRawV1Stream()
        stream.set_rules(follow_usernames=["elonmusk"])
        resp = cache_callback_stream(lambda: stream.get_stream_response(), 0)
        assert resp.status_code == 200

    @responses.activate
    def test_should_handle_failing_return_codes_from_twitter(self):
        """A filtered v1 stream should handle failing return codes from Twitter."""
        responses.add(
            responses.POST,
            "https://stream.twitter.com/1.1/statuses/filter.json",
            json={"error": "not found"},
            status=403,
        )
        stream = FilteredRawV1Stream()
        stream.set_rules(track_keywords=["cats"])
        with pytest.raises(CannotGetStreamException) as e:
            stream.get_stream_response()
        assert "Cannot get stream" in str(e.value)


@vcr_record
@vcr_delete_on_fail
class TestAV2FromV1Strategy:
    """Test: a v2 from v1 strategy..."""

    def test_should_be_able_to_filter_a_tweet_by_keyword(self, v1_data_samples):
        """A v2 from v1 strategy should be able to filter a tweet by keyword."""
        strategy = get_v2_tweet_from_v1_data_strategy({"keywords": ["ingsw2021"]})
        result = strategy(v1_data_samples[0])
        assert result is not None

    def test_should_be_able_to_filter_out_a_tweet_by_keyword(self, v1_data_samples):
        """A v2 from v1 strategy should be able to filter out a tweet by keyword."""
        strategy = get_v2_tweet_from_v1_data_strategy({"keywords": ["not_there"]})
        result = strategy(v1_data_samples[0])
        assert result is None

    def test_should_be_able_to_filter_a_tweet_by_username(self, v1_data_samples):
        """A v2 from v1 strategy should be able to filter a tweet by username."""
        strategy = get_v2_tweet_from_v1_data_strategy({"usernames": ["carlodepieri"]})
        result = strategy(v1_data_samples[0])
        assert result is not None

    def test_should_be_able_to_filter_out_a_tweet_by_username(self, v1_data_samples):
        """A v2 from v1 strategy should be able to filter out a tweet by username."""
        strategy = get_v2_tweet_from_v1_data_strategy({"usernames": ["elonmusk"]})
        result = strategy(v1_data_samples[0])
        assert result is None

    def test_should_be_able_to_filter_a_tweet_by_language(self, v1_data_samples):
        """Av 2 from v 1 strategy should be able to filter a tweet by language."""
        strategy = get_v2_tweet_from_v1_data_strategy({"langs": ["en"]})
        result = strategy(v1_data_samples[0])
        assert result is not None

    def test_should_be_able_to_filter_out_a_tweet_by_language(self, v1_data_samples):
        """A v2 from v1 strategy should be able to filter out a tweet by language."""
        strategy = get_v2_tweet_from_v1_data_strategy({"langs": ["it"]})
        result = strategy(v1_data_samples[0])
        assert result is None

    def test_should_handle_empty_data(self):
        """A v2 from v1 strategy should handle empty data."""
        strategy = get_v2_tweet_from_v1_data_strategy({})
        assert strategy("") is None

    def test_should_ignore_non_tweet_data(self):
        """A v2 from v1 strategy should ignore non tweet data."""
        strategy = get_v2_tweet_from_v1_data_strategy({})
        assert strategy("") is None
        assert strategy('{"message": "not a tweet"}') is None


@vcr_record
@vcr_delete_on_fail
class TestACompetitionStrategy:
    """Test: A competition strategy..."""

    def test_should_handle_non_tweet_data(self):
        """A competition strategy should handle non tweet data."""
        strategy = get_competition_stream_strategy("someid")
        assert strategy("") is None
        assert strategy('{"message": "not a tweet"}') is None

    def test_should_ignore_tweets_not_related_to_the_root_one(self, v1_data_samples):
        """A competition strategy should ignore tweets not related to the root one."""
        strategy = get_competition_stream_strategy("0")
        assert strategy(v1_data_samples[0]) is None

    def test_should_return_the_updated_competition_when_receiving_a_valid_vode(
        self, v1_data_samples
    ):
        """A competition strategy should return the updated competition when receiving a valid vode."""
        vote = v1_data_samples[1]
        tweet_vote = get_tweet_by_id(json.loads(vote)["id"])
        strategy = get_competition_stream_strategy(tweet_vote.conversation_id)
        serialized_competition = strategy(v1_data_samples[1])
        assert "books" in json.loads(serialized_competition)
